import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { DangNhapPage } from '../dang-nhap/dang-nhap';
import { BenhSuPage } from '../benh-su/benh-su';
import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
import { ChiDinhPage } from '../chi-dinh/chi-dinh';
import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
import { KhamBenhPage } from '../kham-benh/kham-benh';
import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';

import { TimKiemPage } from '../tim-kiem/tim-kiem';
import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
import { TrangChuPage } from '../trang-chu/trang-chu';
import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
import { XuTriPage } from '../xu-tri/xu-tri';

/*
  Generated class for the TienCan page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tien-can',
  templateUrl: 'tien-can.html'
})
export class TienCanPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad TienCanPage');
  }

  backPage()
  {
    this.navCtrl.pop();
  }

  saveData()
  {
    this.navCtrl.pop();
  }

}
