import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { DangNhapPage } from '../dang-nhap/dang-nhap';
import { BenhSuPage } from '../benh-su/benh-su';
import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
import { ChiDinhPage } from '../chi-dinh/chi-dinh';
import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
import { KhamBenhPage } from '../kham-benh/kham-benh';
import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
import { TienCanPage } from '../tien-can/tien-can';
import { TimKiemPage } from '../tim-kiem/tim-kiem';
import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
import { TrangChuPage } from '../trang-chu/trang-chu';
import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
import { XuTriPage } from '../xu-tri/xu-tri';
/*
  Generated class for the CacBoPhan page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cac-bo-phan',
  templateUrl: 'cac-bo-phan.html'
})
export class CacBoPhanPage {
  tuanhoan: any;
  hohap: any;
  tieuhoa: any;
  tietnieuSinhduc:any;
  thankinh: any;
  coXuongKhop: any;
  taiMuiHong: any;
  rangHamMat: any;
  mat: any;
  noitiet: any;
  dalieu: any;
  allNormal: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) 
  {
    this.tuanhoan = true;
    this.hohap = true;
    this.tieuhoa = true;
    this.tietnieuSinhduc = true;
    this.thankinh = true;
    this.coXuongKhop = true;
    this.taiMuiHong = true;
    this.rangHamMat = true;
    this.mat = true;
    this.noitiet = true;
    this.dalieu = true;
    this.allNormal = true;
  }

  changeAllNormal() {
    if(this.allNormal == false)
    {
      this.tuanhoan = true;
      this.hohap = true;
      this.tieuhoa = true;
      this.tietnieuSinhduc = true;
      this.thankinh = true;
      this.coXuongKhop = true;
      this.taiMuiHong = true;
      this.rangHamMat = true;
      this.mat = true;
      this.noitiet = true;
      this.dalieu = true;
      this.allNormal = true;
    }

  }

  backPage()
  {
    this.navCtrl.pop();
  }
  
  saveData()
  {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CacBoPhanPage');
  }

}
