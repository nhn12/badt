import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { BenhNhanMoiPage } from '../benh-nhan-moi/benh-nhan-moi';
import { BenhSuPage } from '../benh-su/benh-su';
import { CacBoPhanPage } from '../cac-bo-phan/cac-bo-phan';
import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
import { DangNhapPage } from '../dang-nhap/dang-nhap';
import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
import { ChiDinhPage } from '../chi-dinh/chi-dinh';
import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
import { TienCanPage } from '../tien-can/tien-can';
import { TimKiemPage } from '../tim-kiem/tim-kiem';
import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
import { TrangChuPage } from '../trang-chu/trang-chu';
import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
import { XuTriPage } from '../xu-tri/xu-tri';

/*
  Generated class for the KhamBenh page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-kham-benh',
  templateUrl: 'kham-benh.html'
})
export class KhamBenhPage {
  
  listMenu: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) 
  { 
    let abc :any = this.navCtrl.parent;
    this.listMenu = [new MenuExamination("Lí do yêu cầu khám bệnh", "li-do-yeu-cau-kham-benh"), 
                     new MenuExamination("Bệnh sử", "benh-su"), 
                     new MenuExamination("Tiền căn", "tien-can"), 
                     new MenuExamination("Sinh hiệu", "kham-lam-san")];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad KhamBenhPage');
  }



  menuSelection(itemSelection)
  {
    switch (itemSelection)
    {
      case "li-do-yeu-cau-kham-benh": this.navCtrl.push(LiDoKhamBenhPage);
                                      break;
      case "benh-su": this.navCtrl.push(BenhSuPage);
                      break;
      case "tien-can":  this.navCtrl.push(TienCanPage)
                          break;
      case "kham-lam-san": this.navCtrl.push(SinhHieuPage);
                  break;

    }
  }

  backPage()
  {
    this.navCtrl.parent.select(0);
  }

}

class MenuExamination {
    public nameDisplay: string;
    public valueClick: string;
    constructor(nameDisplay: string, valueClick: string) {
        this.nameDisplay = nameDisplay;
        this.valueClick = valueClick;
    }
    
}
