import { Component , NgZone} from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { SettingServices } from "../../providers/setting-services"
import { RouteRemote } from "../../providers/remote-services"
import { RenderLayout } from "../../providers/render-layout";

/*
  Generated class for the UserSetting page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-user-setting',
  templateUrl: 'user-setting.html'
})
export class UserSettingPage {

  listTab : any;
  dataBenhAnSettings : any;
  dataBenhAnSetting : any ={"length":0, "prefix":""};
  listLengthBenhAn : any[];
  dataBABackup: any;
  dataBackup : any;
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              private settingServices : SettingServices, 
              private zone: NgZone, 
              private loadingCtrl: LoadingController, 
              private remoteServices: RouteRemote,
              private tabLayout : RenderLayout
            ) 
  {
    // this.settingServices.deleteDatabase();
    // this.benhanSetting.deleteDatabase();
    this.listLengthBenhAn = [1,2,3,4,5,6,7,8,9,10,11,12];

    this.settingServices.initDB();

      this.settingServices.getAll() //get tab setting
        .then(data => {
          this.zone.run(() => {
            data.forEach(element => {
                      if (element.name == "dmtacvu") {
                        this.listTab = element.data;
                        this.dataBackup  = element;
                      }

                    });
            //this.listTab = data;
            this.listTab.forEach(element => {
              element.checked = element.show == 1 ? true : false;
            });
          });
        })
        .catch(console.error.bind(console));
        
      this.settingServices.getAll() // get benh an setting
        .then(data => {
          this.zone.run(() => {
            data.forEach(element => {
                      if (element.name == "benhan_setting") {
                        this.dataBenhAnSettings = element.data;
                        this.dataBABackup = element;
                      }

                    });
            //this.dataBenhAnSettings = data;
            this.dataBenhAnSetting = this.dataBenhAnSettings[0];
          });
        })
        .catch(console.error.bind(console));
   
  }

  saveData()
  {
      let loading = this.loadingCtrl.create({
          content: 'Đang lưu',
        });
        loading.present();
      this.settingServices.initDB();
      this.listTab.forEach(element => {
        element.show = element.checked == true ? 1 : 0;
        
      });
      this.dataBackup.data = this.listTab;
      this.dataBackup.Date = new Date();
      this.settingServices.update(this.dataBackup);
      

      this.remoteServices.updateTabSetting(this.listTab).subscribe(data=>{
        this.tabLayout.loadTabs();
        loading.dismiss();
      });

      this.remoteServices.updateBenhAnSetting(this.dataBenhAnSetting).subscribe(data =>{
        
      });

      
      this.dataBenhAnSettings[0]=this.dataBenhAnSetting;
      this.dataBABackup.Date = new Date();
      this.dataBABackup.data = this.dataBenhAnSettings;
      this.settingServices.update(this.dataBABackup);
      

  }

    ionViewDidEnter()
  {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserSettingPage');
  }

}
