import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';

import { TabsPage } from '../tabs/tabs';
//import { DangNhapPage } from '../dang-nhap/dang-nhap';
import { BenhNhanMoiPage } from '../benh-nhan-moi/benh-nhan-moi';
//import { BenhSuPage } from '../benh-su/benh-su';
//import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
//import { ChiDinhPage } from '../chi-dinh/chi-dinh';
//import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
// import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
// import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';

// import { KhamBenhPage } from '../kham-benh/kham-benh';
// import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
// import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
// import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
// import { TienCanPage } from '../tien-can/tien-can';
import { TimKiemPage } from '../tim-kiem/tim-kiem';
// import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
// import { TrangChuPage } from '../trang-chu/trang-chu';
// import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
// import { XuTriPage } from '../xu-tri/xu-tri';
import { CapNhatBenhNhanPage } from '../cap-nhat-benh-nhan/cap-nhat-benh-nhan';

import { Http, Response } from '@angular/http';

;
import { TempData } from "../../providers/temp_data";
import { RouteRemote } from "../../providers/remote-services";

/*
  Generated class for the HanhChinh page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-hanh-chinh',
  templateUrl: 'hanh-chinh.html',
})
export class HanhChinhPage {

  currentPatientId: string;
  currentPatient: any;
  currentMedicalRecordId: any;
  currentMedicalRecord: any;
  currentMedicalRecordNumber: any;
  currentMedicalRecordType: any;
  tempPatients: any[];
  districts : any;
  wards : any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public tempData: TempData, public http: Http, private remoteServices: RouteRemote, public loadingCtrl: LoadingController) {

    this.currentPatient = {
      "mabn": "",
      "holotbn": "",
      "tenbn": "",
      "gioitinh": "",
      "namsinh": ""
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HanhChinhPage');
  }

  ionViewWillEnter() {
    this.http.get("assets/data_example/district.json").subscribe(data => {
      this.districts = data.json(); // get district
    });
    this.http.get("assets/data_example/ward.json").subscribe(data => {
      this.wards = data.json(); // get ward
    });

    let loading = this.loadingCtrl.create({
      content: 'Đang tải'
    });
    loading.present();
    this.currentPatientId = this.tempData.getEntry("currentPatient");
    this.currentMedicalRecordId = this.tempData.getEntry("currentMedicalRecord");
    this.remoteServices.getMedicalRecord(this.currentMedicalRecordId).subscribe(data => {
      this.currentMedicalRecordNumber = data.json().sobenhan;
      this.currentMedicalRecordType = data.json().loaitv;
    });
    this.remoteServices.getPatient(this.currentPatientId).subscribe(data => {
      this.currentPatient = data.json();
      this.currentPatient.phuong = this.getPhuong(this.currentPatient.mapx);
      this.currentPatient.quan = this.getQuan(this.currentPatient.maqh);
      loading.dismiss();
    });
  }

  updatePatient() {
    this.navCtrl.push(CapNhatBenhNhanPage);
  }

  getPhuong(mapx) {
    let phuong : any;
    this.wards.forEach(element => {
      if(element.wardid == mapx)
      {
        phuong = element.name;
      }
    });
    return phuong;
  }

  getQuan(maqh) {
    let quan : any;
    this.districts.forEach(element => {
      if(element.districtid == maqh)
      {
        quan = element.name;
      }
    });
    return quan;
  }

  backPage() {
    this.navCtrl.setRoot(TimKiemPage);
  }
}
