import { Component, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { RouteRemote } from '../../providers/remote-services';
import { SettingServices } from '../../providers/setting-services';
import { TempData } from '../../providers/temp_data'

/*
  Generated class for the ToaThuoc page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-toa-thuoc',
  templateUrl: 'toa-thuoc.html'
})
export class ToaThuocPage {

  shouldShowCancel: any;
  keyQuery: any;
  listMedince: any[];
  listMedinceChoice: any[];
  showSearch: any;
  isExists: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, private remoteServices: RouteRemote, private settingServices: SettingServices, private zone: NgZone, private tempData: TempData) {
    this.listMedinceChoice = [];

    // this.http.get("assets/data_example/medicine.json").subscribe(data => {
    //     console.log("Got data");
    //     this.listMedince=data.json(); // this is the error
    //   });
    this.settingServices.initDB();

    this.settingServices.getAll()
      .then(data => {
        this.zone.run(() => {
          data.forEach(element => {
            if (element.name == "dmduocpham") {
              this.listMedince = element.data;
              this.remoteServices.getChiDinhThuoc(this.tempData.getEntry("currentMedicalRecord")).subscribe(cdts => {
                if (cdts.json().length != 0) {
                  this.isExists = true;
                  cdts.json().forEach(cdt => {
                    this.listMedince.forEach(dp => {
                      if (dp.madp == cdt.madv) {
                        this.listMedinceChoice.push(dp);
                      }
                    });

                  });
                }
                else {

                }


              });
            }
          });
          //   this.listMedince = data;


        });
      })
      .catch(console.error.bind(console));

    this.shouldShowCancel = true;
    this.keyQuery = "";
    this.showSearch = false;
    this.listMedinceChoice = [];
  }

  saveData() {
    if (this.isExists == true) {

    }
    else {
      this.listMedinceChoice.forEach(element => {
        element.idbenhan = this.tempData.getEntry("currentMedicalRecord");
      });
      this.remoteServices.insertChiDinhThuoc(this.listMedinceChoice).subscribe(data=>{

      });
    }
    //this.navCtrl.pop();
  }
  backPage() {
    this.navCtrl.pop();
  }

  onInput(event) {
    if (this.keyQuery == "") {
      this.showSearch = false;
    }
    else {

      this.listMedince.forEach(element => {
        element.soluong = 1;
      });
      this.showSearch = true;

      // }); // case: error not handle

    }

  }
  onCancel(event) {

  }

  testTrigger() {
    this.showSearch = false;
  }

  decreaseMedicine(medicine) {
    if (medicine.soluong > 0) {
      medicine.soluong--;
    }

  }
  increaseMedicine(medicine) {

    medicine.soluong++;

  }

  itemSelected(itemSearch) {
    this.listMedinceChoice.push(itemSearch);
    this.keyQuery = "";
    this.showSearch = false;
  }

  removeItem(item) {
    this.listMedinceChoice.splice(this.listMedinceChoice.indexOf(item), 1);
  }



}

