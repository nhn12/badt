import { Component } from '@angular/core';
import { NavController, NavParams , LoadingController, App, PopoverController } from 'ionic-angular';
// import { AboutPage } from '../about/about';
// import { ContactPage } from '../contact/contact';
// import { HomePage } from '../home/home';
 import { TabsPage } from '../tabs/tabs';
 import { BenhNhanMoiPage } from '../benh-nhan-moi/benh-nhan-moi';
// import { BenhSuPage } from '../benh-su/benh-su';
// import { CacBoPhanPage } from '../cac-bo-phan/cac-bo-phan';
// import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
// import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
// import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
// import { DangNhapPage } from '../dang-nhap/dang-nhap';
// import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
// import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
// import { KhamBenhPage } from '../kham-benh/kham-benh';
// import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
// import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
// import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
// import { TienCanPage } from '../tien-can/tien-can';
// import { TrangChuPage } from '../trang-chu/trang-chu';
// import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
// import { ChiDinhPage } from '../chi-dinh/chi-dinh';
// import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
// import { XuTriPage } from '../xu-tri/xu-tri';
import {Http, Response, Headers, Request, RequestOptions, RequestMethod, URLSearchParams} from '@angular/http';
import {MenuPatientFind} from "../../template/menu-action-find-popover.ts"

import { TempData } from "../../providers/temp_data";
import { RouteRemote } from "../../providers/remote-services"

/*
  Generated class for the TimKiem page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tim-kiem',
  templateUrl: 'tim-kiem.html'
})
export class TimKiemPage {

  shouldShowCancel : any;
  keyQuery:any;
  listPatient:any[];
  showSearch:any;
  hideMessageUnfound : any = true;
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public http:Http, 
              private tempData: TempData, 
              private remoteServices: RouteRemote,
              public loadingCtrl :LoadingController,
              private appCtrl: App,
              private popoverCtrl: PopoverController) 
  {
    
    this.shouldShowCancel =false;
    this.keyQuery = "";
    this.showSearch =false;
  }


  onInput(event) // event input character
  {
      let params: URLSearchParams = new URLSearchParams();
      params.set('query', this.keyQuery);
      //Http request-
      this.remoteServices.searchPatient(this.keyQuery).subscribe(data => {
        this.listPatient = data.json();
        if(this.listPatient.length == 0 && this.keyQuery != "" && this.keyQuery != undefined)
        {
          this.hideMessageUnfound = false;
        }
        else
        {
          this.hideMessageUnfound = true;
        }
      });
  }
  onCancel(event)
  {

  }

  pop(myEvent)
  {
    let popover = this.popoverCtrl.create(MenuPatientFind);
    popover.present({
      ev: myEvent
    });
  }

  backPage()
  {
    this.navCtrl.pop();
  }

  addPatient()
  {
    this.navCtrl.push(BenhNhanMoiPage);
  }

  selectPatient(itemSearch, type)
  {
    let loading = this.loadingCtrl.create({
          content: 'Đang tạo bệnh án mới'
    });
    loading.present();
    ////// Insert Medical Records
    let maBa = getMaBenhAn();
    this.remoteServices.insertMedicalRecord(maBa, itemSearch.mabn, type)
                  .subscribe(data =>{  
                      this.tempData.setEntry("currentPatient", itemSearch.mabn);
                      this.tempData.setEntry("currentMedicalRecord", data.json());
                      while(this.navCtrl.canGoBack()==true)
                      {
                        this.navCtrl.pop();
                      }
                      this.appCtrl.getRootNav().setRoot(TabsPage);
                      loading.dismiss();
                  });
  }
}      

function getMaBenhAn()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for( var i=0; i < 9; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

 
