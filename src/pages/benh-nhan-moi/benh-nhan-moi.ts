import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, App } from 'ionic-angular';
import {Http, Response} from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { TabsPage } from '../tabs/tabs';
// import { DangNhapPage } from '../dang-nhap/dang-nhap';
// import { BenhSuPage } from '../benh-su/benh-su';
// import { CacBoPhanPage } from '../cac-bo-phan/cac-bo-phan';
// import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
// import { ChiDinhPage } from '../chi-dinh/chi-dinh';
// import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
// import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
// import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
// import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
// import { KhamBenhPage } from '../kham-benh/kham-benh';
// import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
// import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
// import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
// import { TienCanPage } from '../tien-can/tien-can';
// import { TimKiemPage } from '../tim-kiem/tim-kiem';
// import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
// import { TrangChuPage } from '../trang-chu/trang-chu';
// import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
// import { XuTriPage } from '../xu-tri/xu-tri';




import { TempData } from "../../providers/temp_data";
import { RouteRemote } from "../../providers/remote-services"
import { RenderLayout } from "../../providers/render-layout"




@Component({
  selector: 'page-benh-nhan-moi',
  templateUrl: 'benh-nhan-moi.html',
})
export class BenhNhanMoiPage {
  
  maBn : any;
  maBa : any;
  districts : any[];
  wards : any[];
  wardsOriginal: any[];
  district : any;
  firstName : any;
  lastName: any;
  ward : any;
  birthday: any;
  address: any;
  gender : any;
  nationality : any;
  isMember : any;
  phone : any;
  itemList : Array<Object>;

  medicalTypes:any[];
  medicalType: any;

  paramAction: any;
  constructor(private appCtrl: App,public navCtrl: NavController, public navParams: NavParams, private http: Http ,private tempData: TempData, private remoteServices : RouteRemote, private renderLayout: RenderLayout,public loadingCtrl: LoadingController) 
  {

     this.maBn = getMaBenhNhan();
     this.maBa = getMaBenhAn();

     this.http.get("assets/data_example/district.json").subscribe(data => {
        console.log("Got data");
        this.districts=data.json(); // get district
      });

      this.http.get("assets/data_example/ward.json").subscribe(data => {
        console.log("Got data");
        this.wardsOriginal=data.json(); // get ward
      });

      this.renderLayout.getTypeMedical().subscribe(data => {
        this.medicalTypes = data.json();
      });

      this.wards=[];

  }
 
  saveData() 
  {
    let loading = this.loadingCtrl.create({
          content: 'Đang tạo bệnh án mới'
        });
        loading.present();
    
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    this.remoteServices.insertPatient(this.maBn, this.firstName, this.lastName, this.birthday, this.gender, this.nationality, this.address, this.district, this.ward, this.phone, this.isMember)
        .subscribe(data =>{
              this.remoteServices.insertMedicalRecord(this.maBa, this.maBn, this.medicalType)
                    .subscribe(data =>{
                          this.tempData.setEntry("currentPatient",this.maBn);
                          this.tempData.setEntry("currentMedicalRecord", data.json());
                          this.navCtrl.setRoot(TabsPage); // switch to HanhChinh tab
                          //this.navCtrl.parent.select(0);
                          this.appCtrl.getRootNav().setRoot(TabsPage);
                          loading.dismiss();
                    });
              
        });

    

  }

  backPage()
  {
    this.navCtrl.pop();
  }

 
  changeDistrict()
  {
    this.wards = [];
    this.wardsOriginal.forEach(element => {
      if(element.districtid == this.district)
      {
        this.wards.push(element);
      }
    });
  }
}

function getMaBenhNhan()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for( var i=0; i < 10; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function getMaBenhAn()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    for( var i=0; i < 9; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}