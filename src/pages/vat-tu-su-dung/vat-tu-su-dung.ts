import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { DangNhapPage } from '../dang-nhap/dang-nhap';
import { BenhSuPage } from '../benh-su/benh-su';
import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
import { ChiDinhPage } from '../chi-dinh/chi-dinh';
import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
import { KhamBenhPage } from '../kham-benh/kham-benh';
import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
import { TienCanPage } from '../tien-can/tien-can';
import { TimKiemPage } from '../tim-kiem/tim-kiem';
import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
import { TrangChuPage } from '../trang-chu/trang-chu';

import { XuTriPage } from '../xu-tri/xu-tri';

/*
  Generated class for the VatTuSuDung page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-vat-tu-su-dung',
  templateUrl: 'vat-tu-su-dung.html'
})
export class VatTuSuDungPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad VatTuSuDungPage');
  }

   saveData(){
    this.navCtrl.pop();
  }
  backPage()
  {
    this.navCtrl.pop();
  }

}
