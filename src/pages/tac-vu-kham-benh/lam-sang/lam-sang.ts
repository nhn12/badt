import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { SettingServices } from "../../../providers/setting-services"
import { RouteRemote } from "../../../providers/remote-services"
import { TempData } from "../../../providers/temp_data";

import { LoadingServices } from "../../../providers/loading-services"
import { NavigationController } from '../../../base-class/NavigationController'
/*
  Generated class for the LamSang page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-lam-sang',
  templateUrl: 'lam-sang.html'
})
export class LamSangPage extends NavigationController {

  listLamSang: any[];
  listLamSangChoice: any[];
  isExists: boolean = false;
  constructor(public appCtrl: App, public navCtrl: NavController,
    public navParams: NavParams,
    private dbServices: SettingServices,
    private zone: NgZone,
    private remoteService: RouteRemote,
    public tempData: TempData,
    private loading: LoadingServices) {
    super(navCtrl, appCtrl);
    this.dbServices.initDB();
    this.listLamSang = [];
    this.dbServices.getAll()
      .then(data => {
        this.zone.run(() => {
          data.forEach(element => {
            if (element.name == "dmbpct") {
              this.listLamSang = element.data;
              this.listLamSang.forEach(element => {
                element.checked = false;
                //element.ngaycs = new Date().getDate();

              });//this.tempData.getEntry("currentMedicalRecord")
              this.remoteService.getLamSang(this.tempData.getEntry("currentMedicalRecord")).subscribe(data => {
                if (data.json().length != 0) {
                  this.isExists = true;
                  data.json().forEach(element1 => {
                    this.listLamSang.forEach(element2 => {
                      if (element2.mabpct == element1.mabpct) {
                        element2.checked = true;
                        element2.chuandoanls = element1.chuandoanls;
                      }
                    });
                  });
                }
              });
            }
          });
        });
      })
      .catch(console.error.bind(console));
  }

  saveData() {
    this.loading.getLoading("Đang lưu").present();
    if (this.isExists == false) {
      this.isExists = true;
      this.listLamSangChoice = [];
      this.listLamSang.forEach(element => {
        if (element.checked == true) {
          element.idbenhan = this.tempData.getEntry("currentMedicalRecord");
          this.listLamSangChoice.push(element);
        }
      });
      this.remoteService.insertLamSang(this.listLamSangChoice).subscribe(data => { this.loading.dismissLoading() });
    }
    else {
      this.listLamSangChoice = [];
      this.listLamSang.forEach(element => {
        if (element.checked == true) {
          element.idbenhan = this.tempData.getEntry("currentMedicalRecord");
          this.listLamSangChoice.push(element);
        }
      });
      this.remoteService.updateLamSang({ "idbenhan": this.tempData.getEntry("currentMedicalRecord"), "bpct": this.listLamSangChoice }).subscribe(data => { this.loading.dismissLoading() });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChiDinhChamSocPage');
  }

}
