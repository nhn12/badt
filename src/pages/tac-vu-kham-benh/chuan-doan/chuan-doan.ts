import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, ModalController, App } from 'ionic-angular';
// import { TabsPage } from '../tabs/tabs';
// import { DangNhapPage } from '../dang-nhap/dang-nhap';
// import { BenhSuPage } from '../benh-su/benh-su';
// import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
// import { ChiDinhPage } from '../chi-dinh/chi-dinh';
// import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
// import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
// import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
// import { KhamBenhPage } from '../kham-benh/kham-benh';
// import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
// import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
// import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
// import { TienCanPage } from '../tien-can/tien-can';
// import { TimKiemPage } from '../tim-kiem/tim-kiem';
// import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
// import { TrangChuPage } from '../trang-chu/trang-chu';
// import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
// import { XuTriPage } from '../xu-tri/xu-tri';
import { Http, Response } from '@angular/http';
import { AutocompleteTextTemplatePage } from '../../../template/autocomplete-text-template/autocomplete-text-template';
import { TempData } from '../../../providers/temp_data';
import { RouteRemote } from '../../../providers/remote-services';
import { SettingServices } from '../../../providers/setting-services';
import { NavigationController } from '../../../base-class/NavigationController';
import { LoadingServices } from '../../../providers/loading-services'


/*
  Generated class for the ChuanDoan page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/

@Component({
  selector: 'page-chuan-doan',
  templateUrl: 'chuan-doan.html'
})
export class ChuanDoanPage extends NavigationController {

  listIcds: any[];
  chuandoan: any;
  chuandoanText: string = "";
  isExists: boolean = false;
  listIcdOriginal: any[];
  constructor(public appCtrl: App, public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    private modalCtrl: ModalController,
    private tempData: TempData,
    private remoteServices: RouteRemote,
    private settingServices: SettingServices,
    private zone: NgZone, private loading: LoadingServices) {
    super(navCtrl, appCtrl);
    this.listIcds = [];
    this.chuandoan = { "chuandoan": " " };
   
    
    this.remoteServices.getChuanDoan(this.tempData.getEntry("currentMedicalRecord")).subscribe(data => {
      if (data.json() != null) {
        this.chuandoan = data.json();
        this.isExists = true;
        this.settingServices.getAll()
          .then(data => {
            this.zone.run(() => {
              data.forEach(element => {
                if (element.name == "dmicd") {
                  this.listIcdOriginal = element.data;
                
                  let icdTemp1 = this.listIcdOriginal.filter(x=>x.maicd.valueOf() == this.chuandoan.maicd1.valueOf());
                  let icdTemp2 = this.listIcdOriginal.filter(x=>x.maicd.valueOf() == this.chuandoan.maicd2.valueOf());
                  if(icdTemp1.length!=0)
                  {
                    this.listIcds.push(icdTemp1[0]);
                  }
                  if(icdTemp2.length!=0)
                  {
                    this.listIcds.push(icdTemp2[0]);
                  }
           
                }
              });
              //   this.listMedince = data;


            });
          })
          .catch(console.error.bind(console));

      }
      else {

      }
    });

  }

  getLengthListICD() {
    if (this.listIcds == undefined) {
      return 0;
    }
    else {
      return this.listIcds.length;
    }

  }

  removeIcd(item) {
    this.listIcds.splice(this.listIcds.indexOf(item), 1);
  }

  saveData() {
    this.loading.getLoading("Đang lưu");
    this.loading.presentLoading();
    if (this.isExists == false) {
      this.chuandoan.idbenhan = this.tempData.getEntry("currentMedicalRecord");
      if (this.listIcds.length == 1) {
        this.chuandoan.maicd1 = this.listIcds[0].maicd;
      }
      if (this.listIcds.length == 2) {
        this.chuandoan.maicd1 = this.listIcds[0].maicd;
        this.chuandoan.maicd2 = this.listIcds[1].maicd;
      }
      this.remoteServices.insertChuanDoan(this.chuandoan).subscribe(data => {
        this.loading.dismissLoading();
      });
    }
    else {
      this.chuandoan.maicd1 = null;
      this.chuandoan.maicd2 = null;
      if (this.listIcds.length == 1) {
        this.chuandoan.maicd1 = this.listIcds[0].maicd;
      }
      if (this.listIcds.length == 2) {
        this.chuandoan.maicd1 = this.listIcds[0].maicd;
        this.chuandoan.maicd2 = this.listIcds[1].maicd;
      }
      this.remoteServices.updateChuanDoan(this.chuandoan).subscribe(data => {
        this.loading.dismissLoading();
      });
    }

    // this.navCtrl.pop();
  }

  onSubmit(items) {
  
  }

  showSearchICDForm() {
    let modal = this.modalCtrl.create(AutocompleteTextTemplatePage, { titlePage: "Tìm kiếm ICD", titleSearch: "Nhập mã/tên IDC  để tìm", sourceData: "dmicd", typeSource: "local", typeAction: "dmicd" });
    let me = this;
    modal.onDidDismiss(data => {
      if(data!= undefined && data!=null)
      {
        this.listIcds.push(data);
      }
      
    });
    modal.present();

  }


}
