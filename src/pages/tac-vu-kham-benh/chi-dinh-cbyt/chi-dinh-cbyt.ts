import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, App } from 'ionic-angular';
import { AutocompleteTextTemplatePage } from "../../../template/autocomplete-text-template/autocomplete-text-template";
import { NavigationController } from '../../../base-class/NavigationController';
import { TempData } from '../../../providers/temp_data';
import {RouteRemote} from '../../../providers/remote-services'
import {LoadingServices} from "../../../providers/loading-services"

/*
  Generated class for the ChiDinhCBYT page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chi-dinh-cbyt',
  templateUrl: 'chi-dinh-cbyt.html'
})
export class ChiDinhCBYTPage extends NavigationController {

  listCBYT: any[];
  defaultCBYT : any;
  isExists:boolean = false;
  constructor(public loading:LoadingServices, public appCtrl:App, public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController, private tempData: TempData, private remoteServices: RouteRemote) {
    super(navCtrl, appCtrl);
    this.listCBYT = [];
    this.defaultCBYT = this.tempData.getEntry("currentCBYT");
    this.remoteServices.getCBYT(this.tempData.getEntry("currentMedicalRecord")).subscribe(data=>{
      if (data.json().length != 0)
      {
        data.json().forEach(element => {
          if(element.macbyt != this.defaultCBYT.macbyt)
          {
            this.listCBYT.push(element);
          }
          
        });;
      }
      
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChiDinhCBYTPage');
  }

  addCBYT() {
    let modal = this.modalCtrl.create(AutocompleteTextTemplatePage, { titlePage: "Tìm kiếm CBYT", titleSearch: "Nhập tên CBYT để tìm", sourceData: this.remoteServices.getDmCBYT(), typeSource: "remote", typeAction: "dmcbyt" });
    let me = this;
    modal.onDidDismiss(data => {
      this.listCBYT.push(data);
    });
    modal.present();           // nhập viện
  }

  saveData()
  {
    this.loading.getLoading("Đang lưu").present();
    if (this.isExists == false) {
      this.isExists = true;
     // this.lisCBYT = [];
      this.listCBYT.forEach(element => {
       
          element.idbenhan = this.tempData.getEntry("currentMedicalRecord");
          //this.listChamSocChoice.push(element);
        
      });
      let tempDataSend = this.listCBYT;
      tempDataSend.push(this.defaultCBYT);
      this.defaultCBYT.idbenhan = this.tempData.getEntry("currentMedicalRecord");
      this.remoteServices.insertCBYT(tempDataSend).subscribe(data => { this.loading.dismissLoading() });
    }
    else {
      //this.listChamSocChoice = [];
      this.listCBYT.forEach(element => {
    
          element.idbenhan = this.tempData.getEntry("currentMedicalRecord");
         // this.listChamSocChoice.push(element);
        
      });
      let tempDataSend = this.listCBYT;
      this.defaultCBYT.idbenhan = this.tempData.getEntry("currentMedicalRecord");
      tempDataSend.push(this.defaultCBYT);
      this.remoteServices.updateCBYT(tempDataSend).subscribe(data => { this.loading.dismissLoading() });
    }
  }

  removeItem(item) {
    this.listCBYT.splice(this.listCBYT.indexOf(item), 1);
  }

}
