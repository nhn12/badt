import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { SettingServices } from "../../../providers/setting-services"
import { RouteRemote } from "../../../providers/remote-services"
import { TempData } from "../../../providers/temp_data";

import { LoadingServices } from "../../../providers/loading-services"
import { NavigationController } from '../../../base-class/NavigationController'
/*
  Generated class for the TienCan page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tien-can',
  templateUrl: 'tien-can.html'
})
export class TienCanPage extends NavigationController {

  listTienCan: any[];
  listTienCanChoice: any[];
  isExists: boolean = false;
  constructor(public appCtrl:App, public navCtrl: NavController,
    public navParams: NavParams,
    private dbServices: SettingServices,
    private zone: NgZone,
    private remoteService: RouteRemote,
    public tempData: TempData,
    private loading: LoadingServices) {
    super(navCtrl, appCtrl);
    this.dbServices.initDB();
    this.listTienCan = [];
    this.dbServices.getAll()
      .then(data => {
        this.zone.run(() => {
          data.forEach(element => {
            if (element.name == "dmtc") {
              this.listTienCan = element.data;
              this.listTienCan.forEach(element => {
                element.checked = false;
                //element.ngaycs = new Date().getDate();

              });//this.tempData.getEntry("currentMedicalRecord")
              this.remoteService.getTienCan(this.tempData.getEntry("currentPatient")).subscribe(data => {
                if (data.json().length != 0) {
                  this.isExists = true;
                  data.json().forEach(element1 => {
                    this.listTienCan.forEach(element2 => {
                      if (element2.matc == element1.matc) {
                        element2.checked = true;
                        element2.ghichu = element1.ghichu;
                      }
                    });
                  });
                }
              });
            }
          });
        });
      })
      .catch(console.error.bind(console));
  }

  saveData() {
    this.loading.getLoading("Đang lưu").present();
    if (this.isExists == false) {
      this.isExists = true;
      this.listTienCanChoice = [];
      this.listTienCan.forEach(element => {
        if (element.checked == true) {
          element.mabn = this.tempData.getEntry("currentPatient");
          this.listTienCanChoice.push(element);
        }
      });
      this.remoteService.insertTienCan(this.listTienCanChoice).subscribe(data => { this.loading.dismissLoading() });
    }
    else {
      this.listTienCanChoice = [];
      this.listTienCan.forEach(element => {
        if (element.checked == true) {
          element.idbenhan = this.tempData.getEntry("currentPatient");
          this.listTienCanChoice.push(element);
        }
      });
      this.remoteService.updateTienCan({ "mabn": this.tempData.getEntry("currentPatient"), "tiencan": this.listTienCanChoice }).subscribe(data => { this.loading.dismissLoading() });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChiDinhChamSocPage');
  }

}
