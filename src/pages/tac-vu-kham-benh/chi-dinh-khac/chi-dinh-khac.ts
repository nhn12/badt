import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the ChiDinhKhac page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chi-dinh-khac',
  templateUrl: 'chi-dinh-khac.html'
})
export class ChiDinhKhacPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChiDinhKhacPage');
  }
  
   saveData(){
    this.navCtrl.pop();
  }
  backPage()
  {
    this.navCtrl.pop();
  }

}
