import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
// import { TabsPage } from '../tabs/tabs';
// import { DangNhapPage } from '../dang-nhap/dang-nhap';
// import { CacBoPhanPage } from '../cac-bo-phan/cac-bo-phan';
// import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
// import { ChiDinhPage } from '../chi-dinh/chi-dinh';
// import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
// import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
// import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
// import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
// import { KhamBenhPage } from '../kham-benh/kham-benh';
// import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
// import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
// import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
// import { TienCanPage } from '../tien-can/tien-can';
// import { TimKiemPage } from '../tim-kiem/tim-kiem';
// import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
// import { TrangChuPage } from '../trang-chu/trang-chu';
// import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
//import { XuTriPage } from '../xu-tri/xu-tri';
import { TempData } from '../../../providers/temp_data';
import { RouteRemote } from '../../../providers/remote-services';
import { NavigationController } from '../../../base-class/NavigationController'
//import {} from '../../providers/'

@Component({
  selector: 'page-benh-su',
  templateUrl: 'benh-su.html'
})
export class BenhSuPage extends NavigationController {

  benhsu: string = "";
  isExists: boolean = false;
  benhsuObject: any;
  textTitleBack: string;
  constructor(public appCtrl: App, public navCtrl: NavController, public navParams: NavParams, private remoteServices: RouteRemote, private tempData: TempData) {
    super(navCtrl, appCtrl);

  }


  ionViewWillEnter() {
    this.remoteServices.getBenhSu(this.tempData.getEntry("currentPatient")).subscribe(data => {
      if (data.json() != null) {
        this.benhsuObject = data.json();
        this.benhsu = this.benhsuObject.benhsu;
        this.isExists = true;
      }
      else {

      }

    });

  }

  saveData() {


    if (this.isExists == true) {
      this.benhsuObject.benhsu = this.benhsu;
      this.remoteServices.updateBenhSu(this.benhsuObject).subscribe(data => { });
    }
    else {
      this.remoteServices.insertBenhSu({ "mabn": this.tempData.getEntry("currentPatient"), "benhsu": this.benhsu }).subscribe(data => { });
    }
    this.navCtrl.pop();
  }

}
