import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, App } from 'ionic-angular';
import{ToaThuocPage} from "../toa-thuoc/toa-thuoc"
import { RouteRemote } from '../../../providers/remote-services';
import { TempData } from '../../../providers/temp_data';
import { AutocompleteTextTemplatePage } from '../../../template/autocomplete-text-template/autocomplete-text-template'
import { NavigationController } from '../../../base-class/NavigationController';
import {LoadingServices} from "../../../providers/loading-services"

@Component({
  selector: 'page-xu-tri',
  templateUrl: 'xu-tri.html'
})
export class XuTriPage extends NavigationController {
  listXutri: any;
  benhvien: any;
  isExists: boolean = false;
  xutri: any;
  taikham: any = { "ngayhentk": new Date() };
  // chuyenvien: any = {"mabv":""};

  constructor(public loading:LoadingServices,public appCtrl:App, public navCtrl: NavController, public navParams: NavParams, private remoteServices: RouteRemote, private modalCtrl: ModalController, private tempData: TempData) {
    super(navCtrl, appCtrl);
    this.taikham.idbenhan = this.tempData.getEntry("currentMedicalRecord");


  }

  ionViewWillEnter() {
    this.remoteServices.getDmXutri().subscribe(data => {
      this.listXutri = data.json();
      this.listXutri.forEach(element => {
        element.checked = false;
      });

          this.remoteServices.getXutri(this.tempData.getEntry("currentMedicalRecord")).subscribe(data1 => {
      if (data1.json() != null) {
        this.isExists = true;
        this.xutri = data1.json();
        this.listXutri.forEach(element => {
          if (element.iddmxutri == this.xutri.xutri) {
            element.checked = true;
            switch (element.iddmxutri) {
              case 1:
              
                break;
              case 2: this.remoteServices.getTaikham(this.tempData.getEntry("currentMedicalRecord")).subscribe(taikham => {
                this.taikham = taikham.json();
              });
                break;
              case 3: this.remoteServices.getNhapvien(this.tempData.getEntry("currentMedicalRecord")).subscribe(taikham => {
                this.benhvien = taikham.json().dmbv;
              });
                break;
            }
          }

        });
      }
      else {
        this.xutri = {};
      }
    });

    });



    this.benhvien = undefined;
  }

  saveData() {
    this.loading.getLoading("Đang lưu");
    this.loading.presentLoading();
    if (this.isExists == false) {
      let idbenhan = this.tempData.getEntry("currentMedicalRecord");
      this.xutri.idbenhan = idbenhan;
      this.listXutri.forEach(element => {
        if (element.checked == true) {
          this.xutri.xutri = element.iddmxutri;
          this.remoteServices.insertXutri(this.xutri).subscribe(data => {

          });
          switch (element.iddmxutri) {
            case 1: //this.navCtrl.push(ToaThuocPage); 
              this.loading.dismissLoading();
              break;
            case 2: this.remoteServices.insertTaikham(this.taikham).subscribe(data => {
                this.loading.dismissLoading();
            });
              break;
            case 3: this.remoteServices.insertNhapvien({ "idbenhan": this.tempData.getEntry("currentMedicalRecord"), "mabv": this.benhvien.mabv }).subscribe(data => {
              this.loading.dismissLoading();
            });
              break;
          }
        }
      });
    }
    else {

    }

  }

  groupCheckbox(item) {
    if (item.checked == false) {

    }
    else {
      this.listXutri.forEach(element => {
        if (element.iddmxutri != item.iddmxutri) {
          element.checked = false;
        }
      });
    }
  }

  actionXutri(item) {
    switch (item.iddmxutri) {
      case 1:  
          this.navCtrl.push(ToaThuocPage);          // cấp toa
        break;
      case 2:            // tái khám
        break;
      case 3: let modal = this.modalCtrl.create(AutocompleteTextTemplatePage, { titlePage: "Tìm kiếm Bệnh viện", titleSearch: "Nhập tên bệnh viện để tìm", sourceData: "", typeSource: "", typeAction: "dmbv" });
        let me = this;
        modal.onDidDismiss(data => {
          this.benhvien = data;
        });
        modal.present();           // nhập viện
        break;
    }
  }

  removeBenhVien()
  {
    this.benhvien = undefined;
  }

}
