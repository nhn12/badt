import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, App, ViewController, PopoverController, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { RouteRemote } from '../../../providers/remote-services';
import { SettingServices } from '../../../providers/setting-services';
import { TempData } from '../../../providers/temp_data'
import { NavigationController } from '../../../base-class/NavigationController'
import { DEFAULT_DIACRITICS } from "../../../providers/unicode-define"
import { LoadingServices } from "../../../providers/loading-services"

@Component({
  selector: 'page-toa-thuoc',
  templateUrl: 'toa-thuoc.html'
})

export class ToaThuocPage extends NavigationController {

  shouldShowCancel: any;
  keyQuery: any;
  listMedinceOriginal: any[];
  listMedinceChoice: any[];
  showSearch: any;
  isExists: boolean = false;
  listMedinceFind: any[] //find result medicine
  delay: any;
  unicodeServices: any;
  constructor(public appCtrl: App,
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: Http,
    private remoteServices: RouteRemote,
    private settingServices: SettingServices,
    private zone: NgZone,
    private tempData: TempData,
    private loading: LoadingServices,
    private popCtrl: PopoverController,
    //private alertCtrl: AlertController
  ) {
    super(navCtrl, appCtrl);
    this.listMedinceChoice = [];

    this.settingServices.initDB();

    this.settingServices.getAll()
      .then(data => {
        this.zone.run(() => {
          data.forEach(element => {
            if (element.name == "dmdp") {
              this.listMedinceOriginal = element.data;
              this.remoteServices.getChiDinhThuoc(this.tempData.getEntry("currentMedicalRecord")).subscribe(cdts => {
                if (cdts.json().length != 0) {
                  this.isExists = true;
                  cdts.json().forEach(cdt => {
                    let listTemp = this.listMedinceOriginal.filter(x => x.madp === cdt.madp);
                    if (listTemp != null && listTemp.length != 0) {
                      this.listMedinceChoice.push(listTemp[0]);
                    }

                  });
                }
                else {

                }


              });
            }
          });
          //   this.listMedince = data;
        });
      })
      .catch(console.error.bind(console));
    this.unicodeServices = new DEFAULT_DIACRITICS();
    this.shouldShowCancel = true;
    this.keyQuery = "";
    this.showSearch = false;
    this.listMedinceChoice = [];

    this.delay = (function () {
      var timer = 0;
      return function (callback, ms) {
        clearTimeout(timer);
        timer = setTimeout(callback, ms);
      };
    })();
  }





  saveData() {
    this.loading.getLoading("Đang lưu");
    this.loading.presentLoading();
    if (this.isExists == true) {
      this.listMedinceChoice.forEach(element => {
        element.idbenhan = this.tempData.getEntry("currentMedicalRecord");
      });
      this.remoteServices.updateChiDinhThuoc({ "idbenhan": this.tempData.getEntry("currentMedicalRecord"), "toathuoc": this.listMedinceChoice }).subscribe(data => {
        this.loading.dismissLoading();
      });
    }
    else {
      this.listMedinceChoice.forEach(element => {
        element.idbenhan = this.tempData.getEntry("currentMedicalRecord");
      });
      this.remoteServices.insertChiDinhThuoc(this.listMedinceChoice).subscribe(data => {
        this.isExists = true;
        this.loading.dismissLoading();

      });
    }
    //this.navCtrl.pop();
  }


  onInput(event) {
    this.listMedinceFind = [];
    this.keyQuery = this.unicodeServices.convertUtf8ToAscii(this.keyQuery);
    if (this.keyQuery == '') {
      this.listMedinceFind = [];

      this.showSearch = false;
      return;
    }
    if (this.keyQuery == "") {
      this.showSearch = false;
    }
    else {

      var self = this;
      this.delay(function () {
        self.listMedinceFind = self.listMedinceOriginal.filter(x => x.search.indexOf(self.keyQuery) != -1);
        if (self.listMedinceFind.length > 20) {
          self.listMedinceFind.splice(20, self.listMedinceFind.length - 20);
        }
      }, 500);
      this.showSearch = true;
    }

  }
  onCancel(event) {

  }

  testTrigger() {
    this.showSearch = false;
  }

  decreaseMedicine(medicine) {
    if (medicine.soluong > 0) {
      medicine.soluong--;
    }

  }
  increaseMedicine(medicine) {

    medicine.soluong++;

  }

  itemSelected(itemSearch) {
    itemSearch.soluong = 1;
    this.listMedinceChoice.push(itemSearch);
    this.keyQuery = "";
    this.showSearch = false;
  }

  removeItem(item) {
    this.listMedinceChoice.splice(this.listMedinceChoice.indexOf(item), 1);
  }

  showExtraAction(ev) {
    let popover = this.popCtrl.create(ExtraActionToaThuoc, { "self": this });
    popover.present({
      ev: ev
    });
  }

  presentAlert() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Chọn nhà thuốc cần gửi?');
    this.remoteServices.getDmNhaThuoc().subscribe(data => {
      data.json().forEach(element => {
        alert.addInput({
          type: 'checkbox',
          label: document.createElement("<p>ancjkdjsdjsjksj</p>"),
          value: 'value1',
          checked: true
        });
      });

      alert.addButton('Cancel');
      alert.addButton({
        text: 'Gửi',
        handler: data => {
        }
      });
      alert.present();
    });


  }

}

@Component({
  template: `
    <ion-list>
      <button ion-item (click)="chuyenToaThuoc()">
            <ion-icon name="ios-send" item-left></ion-icon>
            Lưu & Chuyển toa thuốc
        </button>
        <button ion-item (click)="thamKhaoToaThuoc()">
            <ion-icon name="ios-search" item-left></ion-icon>
            Tham khảo toa thuốc
        </button>
    </ion-list>
  `
})
export class ExtraActionToaThuoc {
  self: any;
  constructor(public viewCtrl: ViewController,
    private parmas: NavParams,
    private remoteServices: RouteRemote,
    private tempData: TempData) {
    this.self = this.parmas.get("self");
  }

  chuyenToaThuoc() {

    this.self.presentAlert();
    this.viewCtrl.dismiss();

  }

  thamKhaoToaThuoc() {

  }

  close() {
    this.viewCtrl.dismiss();
  }
}



