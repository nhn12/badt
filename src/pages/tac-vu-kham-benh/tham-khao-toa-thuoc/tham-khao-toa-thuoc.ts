import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';

import { NavigationController } from '../../../base-class/NavigationController'
/*
  Generated class for the ThamKhaoToaThuoc page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-tham-khao-toa-thuoc',
  templateUrl: 'tham-khao-toa-thuoc.html'
})
export class ThamKhaoToaThuocPage extends NavigationController {

  shouldShowCancel: any;
  keyQuery: any;
  listMedinceChoice: any[];
  showSearch: any;
  constructor(public appCtrl:App, public navCtrl: NavController, public navParams: NavParams) {
    super(navCtrl, appCtrl);
    this.shouldShowCancel = true;
    this.keyQuery = "";
    this.showSearch = false;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ThamKhaoToaThuocPage');
  }

  goTimThuoc() {
    //this.navCtrl.push(ToaThuocPage);
  }
  saveData() {
    //this.navCtrl.push(ToaThuocPage);
  }

  onInput(event) {
    this.showSearch = true;
  }
  onCancel(event) {

  }

  decreaseMedicine(medicine) {


  }
  increaseMedicine(medicine) {



  }

  itemSelected(itemSearch) {

    this.keyQuery = "";
    this.showSearch = false;
  }

}
