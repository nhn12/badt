import { Component } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';

import { Http} from '@angular/http'
import { NavigationController } from '../../../base-class/NavigationController'

/*
  Generated class for the CanLamSang page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-can-lam-sang',
  templateUrl: 'can-lam-sang.html'
})
export class CanLamSangPage extends NavigationController {

  listHuyetHoc: any[];
  listSinhHoas: any[];
  listGans: any[];
  listThans: any[];
  listKhops: any[];
  listMienDichs: any[];
  listUngThus: any[];
  listKiSinhTrungs: any[];
  listViSinhs: any[];
  listNuocTieus: any[];
  listXQuangs: any[];
  listSieuAms: any[];
  listLamSang: any[];
  constructor(public appCtrl: App, public navCtrl: NavController, public navParams: NavParams, public http: Http) {
    super(navCtrl, appCtrl);
    this.http.get("assets/data_example/data.json").subscribe(data => {
      console.log("Got data");
      this.listHuyetHoc = data.json()[0].huyet_hoc; // this is the error
      this.listSinhHoas = data.json()[1].sinh_hoa;
      this.listGans = data.json()[2].gan;
      this.listThans = data.json()[3].than;
      this.listKhops = data.json()[4].khop;
      this.listMienDichs = data.json()[5].mien_dich;
      this.listUngThus = data.json()[6].ung_thu;
      this.listKiSinhTrungs = data.json()[7].ki_sinh_trung;
      this.listNuocTieus = data.json()[9].nuoc_tieu;
      this.listViSinhs = data.json()[10].vi_sinh;
      this.listSieuAms = data.json()[11].sieu_am;
      this.listXQuangs = data.json()[12].x_quang;
    });

    this.listLamSang = [];
    

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CanLamSangPage');
  }


  saveData() {
    this.navCtrl.pop();
  }

  addLamSang() {
    this.listLamSang.push(1);
  }
}
