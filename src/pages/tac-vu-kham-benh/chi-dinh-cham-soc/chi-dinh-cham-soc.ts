import { Component, NgZone } from '@angular/core';
import { NavController, NavParams, App } from 'ionic-angular';
import { SettingServices } from "../../../providers/setting-services"
import { RouteRemote } from "../../../providers/remote-services"
import { TempData } from "../../../providers/temp_data";

import { LoadingServices } from "../../../providers/loading-services"
import { NavigationController } from '../../../base-class/NavigationController';

/*
  Generated class for the ChiDinhChamSoc page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chi-dinh-cham-soc',
  templateUrl: 'chi-dinh-cham-soc.html'
})
export class ChiDinhChamSocPage extends NavigationController {

  listChamSoc: any[];
  listChamSocChoice: any[];
  isExists: boolean = false;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private dbServices: SettingServices,
    private zone: NgZone,
    private remoteService: RouteRemote,
    public tempData: TempData,
    private loading: LoadingServices, 
    public appCtrl: App) {
      super(navCtrl, appCtrl);
    this.dbServices.initDB();
    this.listChamSoc = [];
    this.dbServices.getAll()
      .then(data => {
        this.zone.run(() => {
          data.forEach(element => {
            if (element.name == "dmcs") {
              this.listChamSoc = element.data;
              this.listChamSoc.forEach(element => {
                element.checked = false;
                element.ngaycs = new Date().getDate();

              });//this.tempData.getEntry("currentMedicalRecord")
              this.remoteService.getChiDinhChamSoc(this.tempData.getEntry("currentMedicalRecord")).subscribe(data => {
                if (data.json().length != 0) {
                  this.isExists = true;
                  data.json().forEach(element1 => {
                    this.listChamSoc.forEach(element2 => {
                      if (element2.macs == element1.macs) {
                        element2.checked = true;
                        element2.ngaycs = element1.ngaycs;
                      }
                    });
                  });
                }
              });
            }
          });
        });
      })
      .catch(console.error.bind(console));
  }

  saveData() {
    this.loading.getLoading("Đang lưu").present();
    if (this.isExists == false) {
      this.isExists = true;
      this.listChamSocChoice = [];
      this.listChamSoc.forEach(element => {
        if (element.checked == true) {
          element.idbenhan = this.tempData.getEntry("currentMedicalRecord");
          this.listChamSocChoice.push(element);
        }
      });
      this.remoteService.insertChiDinhChamSoc(this.listChamSocChoice).subscribe(data => { this.loading.dismissLoading() });
    }
    else {
      this.listChamSocChoice = [];
      this.listChamSoc.forEach(element => {
        if (element.checked == true) {
          element.idbenhan = this.tempData.getEntry("currentMedicalRecord");
          this.listChamSocChoice.push(element);
        }
      });
      this.remoteService.updateChiDinhChamSoc({ "idbenhan": this.tempData.getEntry("currentMedicalRecord"), "chamsoc": this.listChamSocChoice }).subscribe(data => { this.loading.dismissLoading() });
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChiDinhChamSocPage');
  }

}
