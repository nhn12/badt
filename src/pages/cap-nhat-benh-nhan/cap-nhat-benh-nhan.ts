import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {Http, Response} from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';


import { TempData } from "../../providers/temp_data";
import { RouteRemote } from "../../providers/remote-services"
import { RenderLayout } from "../../providers/render-layout"
/*
  Generated class for the CapNhatBenhNhan page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cap-nhat-benh-nhan',
  templateUrl: 'cap-nhat-benh-nhan.html',
})
export class CapNhatBenhNhanPage {

  maBn : any;
  soBa: any;
  currentPatientId : any;
  currentMedicalRecordId: any;
  currentMedicalRecordNumber : any;
  currentMedicalRecordType : any;
  currentPatient : any;

  districts: any;
  wardsOriginal: any[];
  medicalTypes :any;
  wards :any;


  constructor(public navCtrl: NavController, public navParams: NavParams, private tempData: TempData, private remoteServices: RouteRemote, private http: Http, private renderLayout: RenderLayout) 
  {
    this.currentPatient = {
      "mabn":"",
      "holotbn":"",
      "tenbn":"",
      "gioitinh":"",
      "namsinh":"",
      "diachibn": "",
      "mapx": "",
      "maqh": "",
      "maqt":"",
      "thanhvien":""
    }

    this.currentPatientId = this.tempData.getEntry("currentPatient");
        this.currentMedicalRecordId = this.tempData.getEntry("currentMedicalRecord");
        this.remoteServices.getMedicalRecord(this.currentMedicalRecordId).subscribe(data =>{
          this.currentMedicalRecordNumber = data.json().sobenhan;
          this.currentMedicalRecordType = data.json().loaitv;
        });
        this.remoteServices.getPatient(this.currentPatientId).subscribe(data => {
            this.currentPatient = data.json();
            this.wards = [];
              this.wardsOriginal.forEach(element => {
                if(element.districtid == this.currentPatient.maqh)
                {
                  this.wards.push(element);
                }
              });
      });

    this.http.get("assets/data_example/district.json").subscribe(data => {
        console.log("Got data");
        this.districts=data.json(); // get district
      });

      this.http.get("assets/data_example/ward.json").subscribe(data => {
        console.log("Got data");
        this.wardsOriginal=data.json(); // get ward
      });

      this.renderLayout.getTypeMedical().subscribe(data => {
        this.medicalTypes = data.json();
      });

      this.wards=[];
  }

    changeDistrict()
      {
        this.wards = [];
        this.wardsOriginal.forEach(element => {
          if(element.districtid == this.currentPatient.maqh)
          {
            this.wards.push(element);
          }
        });
      }
    
  saveData()
  {
    this.remoteServices.updatePatient(this.currentPatient).subscribe(data => {
      this.navCtrl.pop();
    });
  }

  backPage()
  {
    this.navCtrl.pop();
  }


  

  

}
