import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { TabsPage } from '../tabs/tabs';
import { BenhNhanMoiPage } from '../benh-nhan-moi/benh-nhan-moi';
import { BenhSuPage } from '../benh-su/benh-su';
import { CacBoPhanPage } from '../cac-bo-phan/cac-bo-phan';
import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
import { DangNhapPage } from '../dang-nhap/dang-nhap';
import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
import { KhamBenhPage } from '../kham-benh/kham-benh';
import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
import { TienCanPage } from '../tien-can/tien-can';
import { TimKiemPage } from '../tim-kiem/tim-kiem';
import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
import { TrangChuPage } from '../trang-chu/trang-chu';
import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
import { XuTriPage } from '../xu-tri/xu-tri';
/*
  Generated class for the ChiDinh page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chi-dinh',
  templateUrl: 'chi-dinh.html'
})
export class ChiDinhPage {

  listMenu: any[];

  constructor(public navCtrl: NavController, public navParams: NavParams) 
  {
    this.listMenu = [new MenuChiDinh("Cận lâm sàng", "can-lam-san"), 
                     new MenuChiDinh("Thuốc (sử dụng/kê toa)", "toa-thuoc"), 
                     new MenuChiDinh("Vật tư sử dụng", "vat-tu-su-dung"), 
                     new MenuChiDinh("Dịch vụ kĩ thuật", "dich-vu-ki-thuat"),
                     new MenuChiDinh("Chỉ định khác", "chi-dinh-khac")]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChiDinhPage');
  }

  itemSelected(itemSelect)
  {
      switch (itemSelect)
      {
        case 'can-lam-san': this.navCtrl.push(CanLamSangPage);
                            break;
        case 'toa-thuoc': this.navCtrl.push(ThamKhaoToaThuocPage);
                            break;
        case 'vat-tu-su-dung': this.navCtrl.push(VatTuSuDungPage);
                            break;
        case 'dich-vu-ki-thuat': this.navCtrl.push(DichVuKiThuatPage);
                            break;
        case 'chi-dinh-khac': this.navCtrl.push(ChiDinhKhacPage);
                            break;
      }
  }

   saveData(){
      this.navCtrl.pop();
    }
    backPage()
    {
      this.navCtrl.parent.select(0);
    }

}

class MenuChiDinh {
    public nameDisplay: string;
    public valueClick: string;
    constructor(nameDisplay: string, valueClick: string) {
        this.nameDisplay = nameDisplay;
        this.valueClick = valueClick;
    }
}
    
