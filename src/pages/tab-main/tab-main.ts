import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { TimKiemPage } from '../tim-kiem/tim-kiem';
import { UserSettingPage } from '../user-setting/user-setting';

/*
  Generated class for the TabMain tabs.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Component({
  selector: 'page-tab-main',
  templateUrl: 'tab-main.html'
})
export class TabMainPage {
  listTab :any[];
  constructor(public navCtrl: NavController) {
    this.listTab = [];
    this.listTab.push({"title":"Khám bệnh", "Navigation":"TimKiemPage", "icon":"ios-archive"});
    this.listTab.push({"title":"Cài đặt", "Navigation":"UserSettingPage", "icon":"ios-settings"});
    this.listTab.push({"title":"Thông tin", "Navigation":"UserSettingPage", "icon":"ios-information"});

    this.listTab.forEach(element => {
      element.ObjectPage =  this.convertTextToPageObject(element.Navigation);
    });
    
  }





  private convertTextToPageObject(namePage) // convert name page string to object
  {
      
    let objectPage : any;    
    switch (namePage)
    {
      case "TimKiemPage" : objectPage = TimKiemPage; break;
      case "UserSettingPage" : objectPage = UserSettingPage; break;
      default : objectPage = null;
    }
    return objectPage;
  }
  

}