import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { DangNhapPage } from '../dang-nhap/dang-nhap';
import { BenhSuPage } from '../benh-su/benh-su';
import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
import { ChiDinhPage } from '../chi-dinh/chi-dinh';
import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
import { KhamBenhPage } from '../kham-benh/kham-benh';
import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
import { TienCanPage } from '../tien-can/tien-can';
import { TimKiemPage } from '../tim-kiem/tim-kiem';
import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
import { TrangChuPage } from '../trang-chu/trang-chu';
import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
import { RouteRemote } from '../../providers/remote-services';
import { TempData } from '../../providers/temp_data';
import { AutocompleteTextTemplatePage } from '../../template/autocomplete-text-template/autocomplete-text-template'


@Component({
  selector: 'page-xu-tri',
  templateUrl: 'xu-tri.html'
})
export class XuTriPage {
  listXutri: any;
  benhvien: any;
  isExists: boolean = false;
  xutri: any;
  taikham: any ={"ngayhentk": new Date()};
 // chuyenvien: any = {"mabv":""};

  constructor(public navCtrl: NavController, public navParams: NavParams, private remoteServices: RouteRemote, private modalCtrl: ModalController, private tempData: TempData) {
    this.taikham.idbenhan = this.tempData.getEntry("currentMedicalRecord");


  }

  ionViewWillEnter() {
    this.remoteServices.getDmXutri().subscribe(data => {
      this.listXutri = data.json();
      this.listXutri.forEach(element => {
        element.checked = false;
      });

    });


    this.remoteServices.getXutri(this.tempData.getEntry("currentMedicalRecord")).subscribe(data => {
      if (data.json() != null) {
        this.isExists = true;
        this.xutri = data.json();
        this.listXutri.forEach(element => {
          if (element.iddmxutri == this.xutri.xutri) {
            element.checked = true;
            switch (element.iddmxutri) {
            case 1: break;
            case 2: this.remoteServices.getTaikham(this.tempData.getEntry("currentMedicalRecord")).subscribe(taikham=>{
              this.taikham = taikham.json();
            });
                    break;
            case 3: this.remoteServices.getNhapvien(this.tempData.getEntry("currentMedicalRecord")).subscribe(taikham=>{
                      this.benhvien = taikham.json().dmbv;
                    });
                    break;
          }
          }

        });
      }
      else {
        this.xutri = {};
      }
    });
    this.benhvien = undefined;
  }

  saveData() {
    if (this.isExists == false) {
      let idbenhan = this.tempData.getEntry("currentMedicalRecord");
      this.xutri.idbenhan = idbenhan;
      this.listXutri.forEach(element => {
        if (element.checked == true) {
          this.xutri.xutri = element.iddmxutri;
          this.remoteServices.insertXutri(this.xutri).subscribe(data => {

          });
          switch (element.iddmxutri) {
            case 1: break;
            case 2: this.remoteServices.insertTaikham(this.taikham).subscribe(data=>{
              
            });
                    break;
            case 3: this.remoteServices.insertNhapvien({"idbenhan":this.tempData.getEntry("currentMedicalRecord"), "mabv":this.benhvien.mabv}).subscribe(data=>{  
                    });
                    break;
          }
        }
      });
    }
    else{

    }

  }
  backPage() {
    this.navCtrl.parent.select(0);
  }

  groupCheckbox(item) {
    if (item.checked == false) {

    }
    else {
      this.listXutri.forEach(element => {
        if (element.iddmxutri != item.iddmxutri) {
          element.checked = false;
        }
      });
    }
  }

  actionXutri(item) {
    switch (item.iddmxutri) {
      case 1:            // cấp toa
        break;
      case 2:            // tái khám
        break;
      case 3: let modal = this.modalCtrl.create(AutocompleteTextTemplatePage, { titlePage: "Tìm kiếm Bệnh viện", titleSearch: "Nhập tên bệnh viện để tìm", sourceData: "", typeSource: "local", typeAction: "dmbv" });
        let me = this;
        modal.onDidDismiss(data => {
          this.benhvien = data;
        });
        modal.present();           // nhập viện
        break;
    }
  }

}
