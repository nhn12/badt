import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { DangNhapPage } from '../dang-nhap/dang-nhap';
import { BenhSuPage } from '../benh-su/benh-su';
import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
import { ChiDinhPage } from '../chi-dinh/chi-dinh';
import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
import { KhamBenhPage } from '../kham-benh/kham-benh';
import { CacBoPhanPage } from '../cac-bo-phan/cac-bo-phan';
import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
import { TienCanPage } from '../tien-can/tien-can';
import { TimKiemPage } from '../tim-kiem/tim-kiem';
import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
import { TrangChuPage } from '../trang-chu/trang-chu';
import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
import { XuTriPage } from '../xu-tri/xu-tri';

import {TempData} from "../../providers/temp_data";
import { RouteRemote } from "../../providers/remote-services"

/*
  Generated class for the LamSan page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-lam-san',
  templateUrl: 'sinh-hieu.html'
})
export class SinhHieuPage {

  danhSachTriGiac : any;
  isExists:boolean =false;
  // form data
  idsinhhieu: string;
  huyetap2 : string = "/";
  huyetap1 : string;
  mach : string;
  daNiemMac : string;
  nhiptho : string;
  chieucao : string;
  cannang : string;
  khac : string;
  nhietdo : string;
  bmi : string;
  vongnguc : string;
  constructor(public navCtrl: NavController, public navParams: NavParams, private remoteServices: RouteRemote, private loadingCtrl : LoadingController, private tempData : TempData) 
  {

    let currentMedicalRecordId = this.tempData.getEntry("currentMedicalRecord");
    remoteServices.checkDataSinhHieu(currentMedicalRecordId).subscribe(data => {
      if(data.json()==true)
      {
          this.isExists = true;
          remoteServices.getSinhHieu(currentMedicalRecordId).subscribe(data=>{
            
            let sinhhieu = data.json();
            this.idsinhhieu = sinhhieu.idsinhhieu;
            this.mach = sinhhieu.mach;
            this.nhietdo = sinhhieu.nhietdo;
            this.nhiptho = sinhhieu.nhiptho;
            this.huyetap1 = sinhhieu.huyetap1;
            this.huyetap2 = sinhhieu.huyetap2;
            this.vongnguc = sinhhieu.vongnguc;
            this.chieucao = sinhhieu.chieucao;
            this.cannang = sinhhieu.cannang;
            this.bmi = sinhhieu.chisobmi;
        
          });
      }
    });
  }

   saveData(){

    let loading = this.loadingCtrl.create({ //show modal loading
          content: 'Đang lưu'
        });
        loading.present();
    if(this.isExists == false)
    {
      let currentPatientId = this.tempData.getEntry("currentPatient");
      let currentMedicalRecordId = this.tempData.getEntry("currentMedicalRecord");

      this.remoteServices.insertSinhHieu(1, currentMedicalRecordId, this.mach, this.nhiptho, this.nhietdo, this.huyetap1, this.huyetap2, this.vongnguc, this.chieucao, this.cannang, this.bmi)
          .subscribe(data => {
              loading.dismiss();
              if(this.navCtrl.canGoBack())
              {
                this.navCtrl.pop();
              }
              
          });
    }
    else
    {
      let currentPatientId = this.tempData.getEntry("currentPatient");
      let currentMedicalRecordId = this.tempData.getEntry("currentMedicalRecord");

      this.remoteServices.updateSinhHieu(this.idsinhhieu, currentMedicalRecordId, this.mach, this.nhiptho, this.nhietdo, this.huyetap1, this.huyetap2.replace("/", ""), this.vongnguc, this.chieucao, this.cannang, this.bmi)
        .subscribe(data => {
            loading.dismiss();
            this.navCtrl.pop();
        });
    }
    

    


    
  }

  calculateBMI()
  {
    this.bmi = (parseFloat(this.cannang)/(parseFloat(this.chieucao)*parseFloat(this.chieucao))).toString();
  }

  backPage()
  {
    this.navCtrl.pop();
  }

  goBoPhanKhac()
  {
    this.navCtrl.push(CacBoPhanPage);
  }

  keyUpHuyetAp()
  {
    if(this.huyetap2.indexOf("/") != 0)
    {
      this.huyetap2 = "/" + this.huyetap2;
    }
    
  }

}

 class TriGiac {
    public nameDisplay: string;
    public valueClick: string;
    constructor(nameDisplay: string, valueClick: string) {
        this.nameDisplay = nameDisplay;
        this.valueClick = valueClick;
    }
    
}
