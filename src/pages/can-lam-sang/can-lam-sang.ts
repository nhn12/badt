import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { DangNhapPage } from '../dang-nhap/dang-nhap';
import { BenhSuPage } from '../benh-su/benh-su';
import { CacBoPhanPage } from '../cac-bo-phan/cac-bo-phan';
import { ChiDinhPage } from '../chi-dinh/chi-dinh';
import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
import { KhamBenhPage } from '../kham-benh/kham-benh';
import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
import { TienCanPage } from '../tien-can/tien-can';
import { TimKiemPage } from '../tim-kiem/tim-kiem';
import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
import { TrangChuPage } from '../trang-chu/trang-chu';
import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
import { XuTriPage } from '../xu-tri/xu-tri';
import {Http, Response} from '@angular/http'

/*
  Generated class for the CanLamSang page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-can-lam-sang',
  templateUrl: 'can-lam-sang.html'
})
export class CanLamSangPage {

  listHuyetHoc: any[];
  listSinhHoas: any[];
  listGans: any[];
  listThans: any[];
  listKhops: any[];
  listMienDichs: any[];
  listUngThus: any[];
  listKiSinhTrungs: any[];
  listViSinhs: any[];
  listNuocTieus: any[];
  listXQuangs: any[];
  listSieuAms: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http) 
  {
    this.http.get("assets/data_example/data.json").subscribe(data => {
        console.log("Got data");
        let abc: any;
        abc=data.json();
        this.listHuyetHoc=data.json()[0].huyet_hoc; // this is the error
        this.listSinhHoas=data.json()[1].sinh_hoa;
        this.listGans=data.json()[2].gan;
        this.listThans=data.json()[3].than;
        this.listKhops=data.json()[4].khop;
        this.listMienDichs=data.json()[5].mien_dich;
        this.listUngThus=data.json()[6].ung_thu;
        this.listKiSinhTrungs=data.json()[7].ki_sinh_trung;
        this.listNuocTieus=data.json()[9].nuoc_tieu;
        this.listViSinhs=data.json()[10].vi_sinh;
        this.listSieuAms=data.json()[11].sieu_am;
        this.listXQuangs=data.json()[12].x_quang;
    });
      
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CanLamSangPage');
  }
 
 backPage()
 {
   this.navCtrl.pop();
 }

 saveData()
 {
    this.navCtrl.pop();
 }
}
