import { Component, NgZone, trigger, state, style, transition, animate, keyframes } from '@angular/core';
import { RenderLayout } from "../../providers/render-layout";
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { Platform } from 'ionic-angular';

import { TabMainPage } from '../tab-main/tab-main';
import { SettingServices } from "../../providers/setting-services"
// import { BenhNhanMoiPage } from '../benh-nhan-moi/benh-nhan-moi';
// import { BenhSuPage } from '../benh-su/benh-su';
// import { CacBoPhanPage } from '../cac-bo-phan/cac-bo-phan';
// import { CanLamSangPage } from '../can-lam-sang/can-lam-sang';
// import { ChiDinhPage } from '../chi-dinh/chi-dinh';
// import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
// import { ChuanDoanPage } from '../chuan-doan/chuan-doan';
// import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
// import { HanhChinhPage } from '../hanh-chinh/hanh-chinh';
// import { KhamBenhPage } from '../kham-benh/kham-benh';
// import { SinhHieuPage } from '../sinh-hieu/sinh-hieu';
// import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
// import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
// import { TienCanPage } from '../tien-can/tien-can';
import { TimKiemPage } from '../tim-kiem/tim-kiem';
// import { ToaThuocPage } from '../toa-thuoc/toa-thuoc';
// import { TrangChuPage } from '../trang-chu/trang-chu';
// import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
// import { XuTriPage } from '../xu-tri/xu-tri';

import { BenhAnSetting } from "../providers/benhan-setting";
import { LoadingServices } from "../../providers/loading-services"
import { RouteRemote } from "../../providers/remote-services"
import { TempData } from "../../providers/temp_data";



@Component({
  selector: 'page-dang-nhap',
  templateUrl: 'dang-nhap.html',
  animations: [

    //For the logo
    trigger('flyInBottomSlow', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0' }),
        animate('2000ms ease-in-out')
      ])
    ]),

    //For the background detail
    trigger('flyInBottomFast', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        style({ transform: 'translate3d(0,2000px,0)' }),
        animate('1000ms ease-in-out')
      ])
    ]),

    //For the login form
    trigger('bounceInBottom', [
      state('in', style({
        transform: 'translate3d(0,0,0)'
      })),
      transition('void => *', [
        animate('2000ms 200ms ease-in', keyframes([
          style({ transform: 'translate3d(0,2000px,0)', offset: 0 }),
          style({ transform: 'translate3d(0,-20px,0)', offset: 0.9 }),
          style({ transform: 'translate3d(0,0,0)', offset: 1 })
        ]))
      ])
    ]),

    //For login button
    trigger('fadeIn', [
      state('in', style({
        opacity: 1
      })),
      transition('void => *', [
        style({ opacity: 0 }),
        animate('1000ms 2000ms ease-in')
      ])
    ])
  ]
})
export class DangNhapPage {

  //username: any;
  data: any;

  private static TitlePage: string = "DangNhap";

  private dataSetting: any[];
  private dataBenhanSetting: any[];
  private dataTabSetting: any[];

  private numberProcess: number;
  public loadProgress: number = 10;


  logoState: any = "in";
  cloudState: any = "in";
  loginState: any = "in";
  formState: any = "in";

  username: any;
  password: any;
  incorrectLogin:boolean = true;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public loadLayout: RenderLayout,
    private platform: Platform,
    private settingServices: SettingServices,
    private zone: NgZone,
    private remoteServices: RouteRemote,
    private loading: LoadingServices,
    private loadingCtrl: LoadingController,
    private tempData : TempData
  ) {


    let data: any = [{ 'DmName': 'dmdp' }, { 'DmName': 'dmbpct' }, { 'DmName': 'dmicd' }, { 'DmName': 'dmqg' }, { 'DmName': 'dmtacvu' }, { 'DmName': 'dmcs' }, { 'DmName': 'dmxutri' }, { 'DmName': 'benhan_setting' }, { 'DmName': 'dmtc' }];
    // this.remoteServices.getDataUpdate(data).subscribe(data=>{
    // let bac = data.json();
    // });
    this.settingServices.initDB();
    //this.settingServices.deleteDatabase();
    this.synchData(data);
    
  }

  login() {
    this.loading.getLoading("Đăng nhập");
    this.loading.presentLoading();
    this.remoteServices.login({ 'username': this.username, 'password': this.password }).subscribe(data => {
      if (data.json().StatusLogin == true) {
        let value = this.loadLayout.loadTabs();
        this.tempData.setEntry("currentUsername", this.username);
        this.tempData.setEntry("currentCBYT", data.json().Cbyt);
        this.navCtrl.push(TabMainPage);
        this.loading.dismissLoading();
      }
      else
      {
        this.incorrectLogin = false;
        this.loading.dismissLoading();
      }
    });
  }

  private getMaxDate(listItem) {

  }
  synchData(dataSync) {
    this.settingServices.getAll()
      .then(data => {
        this.zone.run(() => {
          if (data == undefined || data.length == 0) // run firstly app
          {
            this.loading.getLoading("Khởi tạo dữ liệu cho lần đầu chạy");
            this.loading.presentLoading();
            this.remoteServices.getDataUpdate(dataSync).subscribe(dataRemote => {
              dataRemote.json().forEach(element => {
                element.time_update = new Date();
                element.Date = new Date();
                this.settingServices.add(element);

              });
              this.loading.dismissLoading();
            });
          }
          else //update data
          {
            this.loading.getLoading("Đang cập nhật dữ liệu");
            this.loading.presentLoading();
            dataSync.forEach(element => {
              element.UpdateTime = data.filter(x => x.name == element.DmName)[0].time_update;
            });
            this.remoteServices.getDataUpdate(dataSync).subscribe(dataRemote => {
              data.forEach(element => {
                let dataTempRemote: any = dataRemote.json().filter(x => x.name == element.name).data;
                if (dataTempRemote != undefined && dataTempRemote != null && dataTempRemote.length != 0) {
                  dataTempRemote.forEach(itemTempRemote => {
                    element.data.push(itemTempRemote);
                  });
                }
                element.time_update = new Date();
                element.Date = new Date();
                this.settingServices.update(element);
              });
              this.loading.dismissLoading();
            });
          }


          // this.dataSetting = data;
          // let flagTab: boolean = false;
          // //let flagBenhan: boolean = false;
          // let localTab: any;
          // //let localBenhan: any;
          // this.dataSetting.forEach(element => {
          //   if (element.name == nameData) {
          //     flagTab = true;
          //     localTab = element;

          //   }
          // });

          // if (flagTab == false) {

          //   srcDataRemote.subscribe(data1 => {
          //     let dataTabRemote = data1.json(); // get data tab setting from tab
          //     console.log(this.dataSetting);
          //     let dataLocal = { "name": nameData, "data": dataTabRemote };
          //     this.settingServices.add(dataLocal);
          //     this.loadProgress = this.loadProgress + 10;
          //   });
          // }
          // else {
          //   dateCheck.subscribe(data => {
          //     let dateUpdate: Date = new Date(data.json());
          //     let op1: any = dateUpdate.valueOf();
          //     let op2: any = (new Date(localTab.data[0].time_update)).valueOf()
          //     if (op1 > op2) // get data from server to local
          //     {
          //       srcDataRemote.subscribe(data2 => {
          //         localTab.Date = new Date();
          //         localTab.data = data2.json();
          //         this.settingServices.update(localTab);
          //       });

          //     }

          //     if (dateUpdate.getMilliseconds() < (new Date(this.dataSetting[0].date_update)).getMilliseconds()) // up data local to server
          //     {

          //     }
          //   });
          // }
        });
      })
      .catch(console.error.bind(console));
  }


  handleError()
  {
    
  }
}
