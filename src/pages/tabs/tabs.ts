// Import angular, ionic lib
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Http, Response } from '@angular/http';

// Import Provider
import { RenderLayout } from "../../providers/render-layout";

// Import Page

//import { ChiDinhPage } from '../chi-dinh/chi-dinh';
//import { ChiDinhKhacPage } from '../chi-dinh-khac/chi-dinh-khac';
import { ChuanDoanPage } from '../tac-vu-kham-benh/chuan-doan/chuan-doan';
//import { DangNhapPage } from '../dang-nhap/dang-nhap';
//import { DichVuKiThuatPage } from '../dich-vu-ki-thuat/dich-vu-ki-thuat';
import { HanhChinhPage } from '../tac-vu-kham-benh/hanh-chinh/hanh-chinh';
//import { KhamBenhPage } from '../tac-vu-kham-benh/kham-benh/kham-benh';
import { SinhHieuPage } from '../tac-vu-kham-benh/sinh-hieu/sinh-hieu';
//import { LiDoKhamBenhPage } from '../li-do-kham-benh/li-do-kham-benh';
//import { ThamKhaoToaThuocPage } from '../tham-khao-toa-thuoc/tham-khao-toa-thuoc';
import { TienCanPage } from '../tac-vu-kham-benh/tien-can/tien-can';
//import { TimKiemPage } from '../tim-kiem/tim-kiem';
import { ToaThuocPage } from '../tac-vu-kham-benh/toa-thuoc/toa-thuoc';
//import { TrangChuPage } from '../tac-vu-kham-benh/trang-chu/trang-chu';
//import { VatTuSuDungPage } from '../vat-tu-su-dung/vat-tu-su-dung';
import { XuTriPage } from '../tac-vu-kham-benh/xu-tri/xu-tri';
import { BenhSuPage } from '../tac-vu-kham-benh/benh-su/benh-su';
import { TacVuPage } from '../tac-vu/tac-vu';
import { ChiDinhCBYTPage } from '../tac-vu-kham-benh/chi-dinh-cbyt/chi-dinh-cbyt'
import { ChiDinhChamSocPage } from "../tac-vu-kham-benh/chi-dinh-cham-soc/chi-dinh-cham-soc";
import { TacVuChamSocPage } from "../tac-vu-kham-benh/tac-vu-cham-soc/tac-vu-cham-soc";
import { CanLamSangPage } from "../tac-vu-kham-benh/can-lam-sang/can-lam-sang";
import { LamSangPage } from "../tac-vu-kham-benh/lam-sang/lam-sang";




@Component({
  templateUrl: 'tabs.html',
})
export class TabsPage {

  private listTabs: any[];
  private listTabsOrginal: any[];
  tabDemo: any;

  constructor(public navCtrl: NavController, public http: Http, public loadLayout: RenderLayout) {
    this.listTabsOrginal = loadLayout.getTab(); // get tab from json
    this.listTabs = [];
    this.listTabsOrginal.forEach(element => {
      element.ObjectPage = this.convertTextToPageObject(element.idtacvu);
      if (element.show == 1) {
        this.listTabs.push(element);
      }
    });
    if (this.listTabsOrginal.length > this.listTabs.length) {
      this.listTabs.push({ "tentacvu": "Thêm", "icon": "ios-more", "ObjectPage": TacVuPage });
    }
    //this.tabDemo = TrangChuPage;
  }

  ionChange() {
    //this.listTabs.pop();
  }

  showMoreAction() {

  }


  private convertTextToPageObject(namePage) // convert name page string to object
  {

    let objectPage: any;
    switch (namePage) {
      case "ttcddp": objectPage = ToaThuocPage; break;
     // case "KhamBenhPage": objectPage = KhamBenhPage; break;
      //case "ChiDinhPage": objectPage = ChiDinhPage; break;
      case "ttchuandoan": objectPage = ChuanDoanPage; break;
      //case "TrangChuPage": objectPage = TrangChuPage; break;
      case "ttxutri": objectPage = XuTriPage; break;
      case "tthanhchinh": objectPage = HanhChinhPage; break;
      case "ttsinhhieu": objectPage = SinhHieuPage; break;
      case "ttbenhsu": objectPage = BenhSuPage; break;
      case "ttcdcbyt": objectPage = ChiDinhCBYTPage; break;
      case "ttcdcs": objectPage = ChiDinhChamSocPage; break;
      case "tttvcs": objectPage = TacVuChamSocPage; break;
      case "ttcls": objectPage = CanLamSangPage; break;
      case "ttlamsang": objectPage = LamSangPage; break;
      case "tttiencan": objectPage = TienCanPage; break;
      default: objectPage = null;
    }
    return objectPage;
  }
}

