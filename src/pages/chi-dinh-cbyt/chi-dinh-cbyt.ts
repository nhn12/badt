import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import {AutocompleteTextTemplatePage} from "../../template/autocomplete-text-template/autocomplete-text-template"

/*
  Generated class for the ChiDinhCBYT page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-chi-dinh-cbyt',
  templateUrl: 'chi-dinh-cbyt.html'
})
export class ChiDinhCBYTPage {

  listCBYT: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, private modalCtrl: ModalController) 
  {
    this.listCBYT=[];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChiDinhCBYTPage');
  }

  addCBYT()
  {
    let modal = this.modalCtrl.create(AutocompleteTextTemplatePage, { titlePage: "Tìm kiếm CBYT", titleSearch: "Nhập tên CBYT để tìm", sourceData: "", typeSource: "local", typeAction : "dmcbyt" });
              let me = this;
              modal.onDidDismiss(data => {
                this.listCBYT.push(data);
              });
              modal.present();           // nhập viện
  }

  removeItem(item)
  {
     this.listCBYT.splice(this.listCBYT.indexOf(item), 1);
  }

}
