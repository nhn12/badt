import { Injectable } from '@angular/core';
import { Http, Response, Headers, Request, RequestOptions, RequestMethod, URLSearchParams } from '@angular/http';
import { ROUTE } from "../providers/const-route.ts"
import 'rxjs/add/operator/map';

@Injectable()
export class RouteRemote {

  constructor(public http: Http) {

  }

  public getAllPatients() {
    let params: URLSearchParams = new URLSearchParams();

    return this.http.get(ROUTE.WEB_API + ROUTE.GET_ALL_PATIENTS, { search: params });
  }

  public getPatient(maBn) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('mabn', maBn);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_PATIENT, { search: params });
  }

  public getAllMedicalRecords() {
    this.http.get(ROUTE.WEB_API + ROUTE.GET_ALL_MEDICAL_RECORDS);
  }

  public getMedicalRecord(maBa) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('maBa', maBa);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_MEDICAL_RECORD, { search: params });
  }

  public insertMedicalRecord(sobenhan, maBn, loatv) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_MEDICAL_RECORD, { "sobenhan": sobenhan, "mabn": maBn, "loaitv": loatv }, options);
  }

  public insertPatient(maBn, firstName, lastName, birthday, gender, nationality, address, district, ward, phone, isMember) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_PATIENT,
      {
        "mabn": maBn, "holotbn": firstName, "tenbn": lastName,
        "namsinh": birthday, "gioitinh": gender,
        "thanhvien": isMember == true ? 1 : 0, "maqt": nationality, "diachibn": address,
        "matt": "79", "maqh": district, "mapx": ward, "dienthoai": phone
      }
      , options)
  }

  public updatePatient(newPatient) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(newPatient);

    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_PATIENT,
      dta
      , options)
  }

  public searchPatient(query) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('query', query);
    return this.http.get(ROUTE.WEB_API + ROUTE.SEARCH_PATIENT, { search: params });
  }

  public getSinhHieu(maBa) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('maba', maBa);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_SINHHIEU, { search: params });
  }

  public insertSinhHieu(idsinhhieu, idbenhan, mach, nhiptho, nhietdo, huyetap1, huyetap2, vongnguc, chieucao, cannang, chisobmi) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_SINHHIEU,
      {
        "idsinhhieu": idsinhhieu, "idbenhan": idbenhan, "mach": mach,
        "nhiptho": nhiptho, "nhietdo": nhietdo,
        "huyetap1": huyetap1, "huyetap2": huyetap2, "vongnguc": vongnguc,
        "chieucao": chieucao, "cannang": cannang, "chisobmi": chisobmi
      }
      , options);
  }

  public updateSinhHieu(idsinhhieu, idbenhan, mach, nhiptho, nhietdo, huyetap1, huyetap2, vongnguc, chieucao, cannang, chisobmi) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_SINHHIEU,
      {
        "idsinhhieu": idsinhhieu, "idbenhan": idbenhan, "mach": mach,
        "nhiptho": nhiptho, "nhietdo": nhietdo,
        "huyetap1": huyetap1, "huyetap2": huyetap2, "vongnguc": vongnguc,
        "chieucao": chieucao, "cannang": cannang, "chisobmi": chisobmi
      }
      , options);
  }

  public checkDataSinhHieu(maBa) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('maba', maBa);
    return this.http.get(ROUTE.WEB_API + ROUTE.CHECK_DATASINHHIEU, { search: params });
  }

  public getTabSetting() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_TABSETTING);
  }

  public updateTabSetting(dmtv) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(dmtv);

    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_TABSETTING, dta, options);
  }

  public getDateUpdateTabSetting() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GETDATEUPDATE_TABSETTING);
  }

  public getBenhAnSetting() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_BENHANSETTING);
  }

  public updateBenhAnSetting(benhan) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(benhan);

    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_BENHANSETTING, dta, options);
  }

  public getDateUpdateBenhAnSetting() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GETDATEUPDATE_BENHANSETTING);
  }

  public getDmICD() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_ICD);
  }

  public searchDmICD(query) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('keyQuery', query);
    return this.http.get(ROUTE.WEB_API + ROUTE.SEARCH_DM_ICD, { search: params });
  }

  public getDateUpdateDmIcd() {
    return this.http.get(ROUTE.WEB_API + ROUTE.CHECK_DM_ICD);
  }

  public getChuanDoan(maba: string) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('maba', maba);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_CHUANDOAN, { search: params });
  }
  public insertChuanDoan(chuandoan) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(chuandoan);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_CHUANDOAN,
      dta
      , options)

  }

  public updateChuanDoan(chuandoan) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(chuandoan);

    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_CHUANDOAN, dta, options)

  }

  public getDmXutri() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_XUTRI);
  }

  public getXutri(maba) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('idbenhan', maba);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_XUTRI, { search: params });
  }

  public insertXutri(xutri) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(xutri);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_XUTRI,
      dta
      , options)

  }

  public updateXutri(xutri) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(xutri);

    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_XUTRI, dta, options)

  }

  public getTaikham(maba) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('maba', maba);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_TAIKHAM, { search: params });
  }

  public insertTaikham(taikham) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(taikham);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_TAIKHAM,
      dta
      , options)
  }

  public updateTaikham(taikham) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(taikham);

    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_TAIKHAM, dta, options)

  }


  public getNhapvien(maba) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('idbenhan', maba);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_BENHVIEN, { search: params });
  }

  public insertNhapvien(nhapvien) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(nhapvien);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_BENHVIEN,
      dta
      , options)
  }

  public updateNhapvien(nhapvien) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(nhapvien);

    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_BENHVIEN, dta, options)

  }

  public getDateUpdateDmXutri() {
    return this.http.get(ROUTE.WEB_API + ROUTE.CHECK_DM_XUTRI);
  }

  public searchDmBenhVien(query) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('keyQuery', query);
    return this.http.get(ROUTE.WEB_API + ROUTE.SEARCH_DM_BENHVIEN, { search: params });
  }

  public searchDmCBYT(query) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('keyQuery', query);
    return this.http.get(ROUTE.WEB_API + ROUTE.SEARCH_DM_CBYT, { search: params });
  }

  public searchDmDuocPham(query) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('keyQuery', query);
    return this.http.get(ROUTE.WEB_API + ROUTE.SEARCH_DM_DUOCPHAM, { search: params });
  }

  public getDmDuocPham() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_DUOCPHAM);
  }

  public getDateUpdateDuocPhamSetting() {
    return this.http.get(ROUTE.WEB_API + ROUTE.CHECK_DM_DUOCPHAM);
  }

  public getDmDonViTinh() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_DONVITINH);
  }

  public getDateUpdateDonViTinh() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DATE_DM_DONVITINH);
  }

  //   public getDmDuongDungDuocPham() {
  //   return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_DONVITINH);
  // }

  // public getDateUpdateDuongDungDuocPham() {
  //   return this.http.get(ROUTE.WEB_API + ROUTE.GET_DATE_DM_DONVITINH);
  // }

  public getBenhSu(mabn: string) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('benhnhan', mabn);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_BENHSU, { search: params });
  }

  public insertBenhSu(benhsu: any) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(benhsu);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_BENHSU,
      dta
      , options)

  }

  public updateBenhSu(benhsu: any) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(benhsu);

    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_BENHSU,
      dta
      , options)

  }

  public getChiDinhThuoc(maba) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('idbenhan', maba);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_CHIDINHTHUOC, { search: params });
  }

    public sendMailToaThuoc(maba) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('maba', maba);
    return this.http.get(ROUTE.WEB_API + ROUTE.SENDMAIL_TOATHUOC, { search: params });
  }

  public insertChiDinhThuoc(cdt) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(cdt);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_CHIDINHTHUOC,
      dta
      , options)
  }

  public updateChiDinhThuoc(cdt) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let dta = JSON.stringify(cdt);
    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_CHIDINHTHUOC, dta, options)

  }

  public getDmChamSoc() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_CHAMSOC);
  }

  public getDateUpdateChamSoc() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DATE_DM_CHAMSOC);
  }

  public getChiDinhChamSoc(maba) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('maba', maba);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_CHIDINHCHAMSOC, { search: params });
  }

  public insertChiDinhChamSoc(chamsoc) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(chamsoc);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_CHIDINHCHAMSOC,
      dta
      , options)
  }

  public updateChiDinhChamSoc(chamsoc) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let dta = JSON.stringify(chamsoc);
    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_CHIDINHCHAMSOC, dta, options)

  }

  public getTacVuChamSoc(maba) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('maba', maba);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_TACVUCHAMSOC, { search: params });
  }

  public insertTacVuChamSoc(chamsoc) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(chamsoc);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_TACVUCHAMSOC,
      dta
      , options)
  }

  public updateTacVuChamSoc(chamsoc) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let dta = JSON.stringify(chamsoc);
    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_TACVUCHAMSOC, dta, options)

  }

  public getLamSang(maba) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('maba', maba);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_LAMSANG, { search: params });
  }

  public getDmBoPhanCoThe() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_BOPHANCOTHE);
  }

  public getDateUpdateBoPhanCoThe() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DATE_DM_BOPHANCOTHE);
  }

  public insertLamSang(lamsang) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(lamsang);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_LAMSANG,
      dta
      , options)
  }

  public updateLamSang(lamsang) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let dta = JSON.stringify(lamsang);
    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_LAMSANG, dta, options)

  }

  public getTienCan(mabn) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('mabn', mabn);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_TIENCAN, { search: params });
  }

  public getDmTienCan() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_TIENCAN);
  }

  public getDateUpdateTienCan() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DATE_DM_TIENCAN);
  }

  public insertTienCan(tiencan) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(tiencan);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_TIENCAN,
      dta
      , options)
  }

  public updateTienCan(tiencan) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let dta = JSON.stringify(tiencan);
    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_TIENCAN, dta, options)

  }

  public getDmCanLamSang() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_CANLAMSANG);
  }

  public getDateUpdateCanLamSang() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DATE_DM_CANLAMSANG);
  }


  public getDmQuocGia() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_QUOCGIA);
  }
  public getDateUpdateQuocGia() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DATE_DM_QUOCGIA);
  }

  public getDataUpdate(cdt) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(cdt);

    return this.http.post(ROUTE.WEB_API + ROUTE.GET_DATA_UPDATE_DANHMUC,
      dta
      , options)
  }

  public login(account: any) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(account);

    return this.http.post(ROUTE.WEB_API + ROUTE.LOGIN,
      dta, options);

  }

  public getDmCBYT() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_CBYT);
  }

  public getCBYT(maba) {
    let params: URLSearchParams = new URLSearchParams();
    params.set('maba', maba);
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_CBYT, { search: params });
  }

  public insertCBYT(tiencan) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    let dta = JSON.stringify(tiencan);

    return this.http.post(ROUTE.WEB_API + ROUTE.INSERT_CBYT,
      dta
      , options)
  }

  public updateCBYT(tiencan) {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    let dta = JSON.stringify(tiencan);
    return this.http.post(ROUTE.WEB_API + ROUTE.UPDATE_CBYT, dta, options)

  }

    public getDmNhaThuoc() {
    return this.http.get(ROUTE.WEB_API + ROUTE.GET_DM_NHATHUOC);
  }
}
