import { Injectable } from '@angular/core';
import * as PouchDB from 'pouchdb';

@Injectable()
export class BenhAnSetting {
  private _db;
  private _data;

  initDB() {
    this._db = new PouchDB('BenhAnSetting', { adapter: 'websql' });
    window["PouchDB"] = PouchDB;
  }

  add(item) {
    return this._db.post(item);
  }
  update(item) {
    return this._db.put(item);
  }

  delete(item) {
    return this._db.remove(item);
  }

  deleteDatabase() {
    var db = new PouchDB('BenhAnSetting');
    db.destroy().then(function () { console.log('ALL YOUR BASE ARE BELONG TO US') });
  }

  getAll() {

    if (!this._data) {
      return this._db.allDocs({ include_docs: true })
        .then(docs => {

          this._data = docs.rows.map(row => {
            // Dates are not automatically converted from a string.
            row.doc.Date = new Date(row.doc.Date);
            return row.doc;
          });

          // Listen for changes on the database.
          this._db.changes({ live: true, since: 'now', include_docs: true })
            .on('change', this.onDatabaseChange);

          return this._data;
        });
    } else {
      // Return cached data as a promise
      return Promise.resolve(this._data);
    }
  }

  private onDatabaseChange = (change) => {
    var index = this.findIndex(this._data, change.id);
    var birthday = this._data[index];

    if (change.deleted) {
      if (birthday) {
        this._data.splice(index, 1); // delete
      }
    } else {
      change.doc.Date = new Date(change.doc.Date);
      if (birthday && birthday._id === change.id) {
        this._data[index] = change.doc; // update
      } else {
        this._data.splice(index, 0, change.doc) // insert
      }
    }
  }

  private findIndex(array, id) {
    var low = 0, high = array.length, mid;
    while (low < high) {
      mid = (low + high) >>> 1;
      array[mid]._id < id ? low = mid + 1 : high = mid
    }
    return low;
  }
}
