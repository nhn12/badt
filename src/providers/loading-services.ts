import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class LoadingServices {
  loading: any;
  constructor(public http: Http, private loadingCtrl: LoadingController) {
    this.loading = this.loadingCtrl.create({

     // duration: 10
    });
  }

  getLoading(titleLoading) {
    
    this.loading.setContent(titleLoading);
    return this.loading;
  }

  dismissLoading() {
    this.loading.dismiss();
    this.loading = this.loadingCtrl.create({

     // duration: 10
    });
  }

  presentLoading()
  {
    this.loading.present();
  }

}
