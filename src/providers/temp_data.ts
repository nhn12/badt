import { Injectable } from '@angular/core';
import {Platform} from 'ionic-angular';
import {StatusBar} from 'ionic-native';
 
@Injectable()
export class TempData {
    data : GenericType[];
    constructor() {
    this.data = [];
  }
  setEntry(key : string, value: any)
  {
    this.data.forEach(element => {
       if(element.key == key)
       {
         element.value = value;
         return;
       }
    });
    this.data.push(new GenericType(key, value));
  }
  getEntry(key:string)
  {
     let itemSelection : any;
      this.data.forEach(element => {
        if(element.key == key)
       {
         itemSelection =  element.value;
         
       }
      });
      return itemSelection;
  }
}

class GenericType
{
   key:string;
   value: string;
   constructor(key:string, value:string)
   {
      this.key = key;
      this.value = value;
   }
}