
export class ROUTE {

  public static WEB_API: string = "http://54.191.101.251:3001/";
  // Remote
  public static GET_DATA_UPDATE_DANHMUC: string ="CheckUpdate/CheckDateUpdateAll"
  public static GET_ALL_PATIENTS: string = "benhnhan/abcbenhnhans";
  public static GET_PATIENT: string = "benhnhan/abcbenhnhan";
  public static GET_ALL_MEDICAL_RECORDS: string = "benhan/getbenhans";
  public static UPDATE_PATIENT: string = "benhnhan/UpdateBenhNhan";
  public static GET_MEDICAL_RECORD: string = "benhan/getbenhan";
  public static INSERT_PATIENT: string = "benhnhan/postbenhnhan";
  public static INSERT_MEDICAL_RECORD: string = "benhan/postbenhan";
  public static SEARCH_PATIENT: string = "benhnhan/searchbenhnhan";

  public static GET_SINHHIEU: string = "sinhhieu/getsinhhieu";
  public static INSERT_SINHHIEU: string = "sinhhieu/postsinhhieu";
  public static UPDATE_SINHHIEU: string = "sinhhieu/updatesinhhieu";
  public static CHECK_DATASINHHIEU: string = "sinhhieu/CheckExistsSinhHieu";

  public static GET_TABSETTING: string = "datasetting/GetSettingTab";
  public static UPDATE_TABSETTING: string = "datasetting/UpdateTabSetting";
  public static GETDATEUPDATE_TABSETTING: string = "datasetting/GetDateUpdateTabSetting";

  public static GET_BENHANSETTING: string = "datasetting/GetSettingbenhan";
  public static UPDATE_BENHANSETTING: string = "datasetting/UpdateBenhanSetting";
  public static GETDATEUPDATE_BENHANSETTING: string = "datasetting/GetDateUpdateBenhanSetting";

  public static GET_DM_ICD: string = "danhmuc/geticd";
  public static SEARCH_DM_ICD: string = "danhmuc/searchicd";
  public static GET_CHUANDOAN: string = "chuandoan/getchuandoan";
  public static INSERT_CHUANDOAN: string = "chuandoan/insertchuandoan";
  public static UPDATE_CHUANDOAN: string = "chuandoan/updatechuandoan";
  public static CHECK_DM_ICD: string = "CheckUpdate/CheckDateICD";

  public static GET_DM_XUTRI: string = "danhmuc/getxutri";
  public static GET_XUTRI: string = "xutri/getxutri";
  public static INSERT_XUTRI: string = "xutri/insertxutri";
  public static UPDATE_XUTRI: string = "xutri/updatexutri";
  public static CHECK_DM_XUTRI: string = "CheckUpdate/CheckDateXutri";

  public static SEARCH_DM_BENHVIEN: string = "danhmuc/searchbenhvien";
  public static GET_BENHVIEN: string = "BenhVien/getBenhVien";
  public static INSERT_BENHVIEN: string = "BenhVien/insertBenhVien";
  public static UPDATE_BENHVIEN: string = "BenhVien/updateBenhVien";


  public static SEARCH_DM_DUOCPHAM: string = "danhmuc/searchDmDuocPham";
  public static GET_DM_DUOCPHAM: string = "danhmuc/getDmDuocPham";
  public static CHECK_DM_DUOCPHAM: string = "CheckUpdate/CheckDateDuocPham";
  public static GET_DM_DONVITINH: string = "danhmuc/getdmdonvitinh";
  public static GET_DATE_DM_DONVITINH: string = "CheckUpdate/CheckDatedonvitinh";
  public static GET_DM_DUONGDUNGDUOCPHAM: string = "danhmuc/getdmduongdungduocpham";
  public static GET_DATE_DM_DUONGDUNGDUOCPHAM: string = "CheckUpdate/CheckDateduongdungduocpham";


  public static GET_BENHSU: string = "benhsu/getbenhsu";
  public static INSERT_BENHSU: string = "benhsu/insertbenhsu";
  public static UPDATE_BENHSU: string = "benhsu/updatebenhsu";

  public static GET_TAIKHAM: string = "taikham/gettaikham";
  public static INSERT_TAIKHAM: string = "taikham/inserttaikham";
  public static UPDATE_TAIKHAM: string = "taikham/updatetaikham";

  public static GET_CHIDINHTHUOC: string = "ChiDinhThuoc/getChiDinhThuoc";
  public static INSERT_CHIDINHTHUOC: string = "ChiDinhThuoc/insertChiDinhThuoc";
  public static UPDATE_CHIDINHTHUOC: string = "ChiDinhThuoc/updateChiDinhThuoc";
  public static SENDMAIL_TOATHUOC: string = "ChiDinhThuoc/sendmail";

  public static GET_DM_CHAMSOC: string = "danhmuc/GetDmChamSoc";
  public static GET_DATE_DM_CHAMSOC: string = "CheckUpdate/CheckDateChamSoc";
  public static GET_CHIDINHCHAMSOC: string = "chamsoc/getchidinhchamsoc";
  public static INSERT_CHIDINHCHAMSOC: string = "chamsoc/insertchidinhchamsoc";
  public static UPDATE_CHIDINHCHAMSOC: string = "chamsoc/updatechidinhchamsoc";
  public static GET_TACVUCHAMSOC: string = "chamsoc/gettacvuchamsoc";
  public static INSERT_TACVUCHAMSOC: string = "chamsoc/inserttacvuchamsoc";
  public static UPDATE_TACVUCHAMSOC: string = "chamsoc/updatetacvuchamsoc";

  public static GET_DM_BOPHANCOTHE: string = "danhmuc/getdmbophancothe";
  public static GET_DATE_DM_BOPHANCOTHE: string = "CheckUpdate/CheckDateBoPhanCoThe";
  public static GET_LAMSANG: string = "lamsang/getlamsang";
  public static INSERT_LAMSANG: string = "lamsang/insertlamsang";
  public static UPDATE_LAMSANG: string = "lamsang/updatelamsang";

  public static GET_DM_TIENCAN: string = "danhmuc/getdmtiencan";
  public static GET_DATE_DM_TIENCAN: string = "CheckUpdate/CheckDatetiencan";
  public static GET_TIENCAN: string = "tiencan/gettiencan";
  public static INSERT_TIENCAN: string = "tiencan/inserttiencan";
  public static UPDATE_TIENCAN: string = "tiencan/updatetiencan";

  public static GET_DM_CANLAMSANG: string = "danhmuc/getdmcanlamsang";
  public static GET_DATE_DM_CANLAMSANG: string = "CheckUpdate/CheckDateCanLamSang";


  public static GET_DM_QUOCGIA: string = "danhmuc/getdmquocgia";
  public static GET_DATE_DM_QUOCGIA: string = "CheckUpdate/CheckDateQuocGia";

  public static LOGIN: string = "login/login";

  public static SEARCH_DM_CBYT: string = "danhmuc/searchDmCBYT";
  public static GET_DM_CBYT: string = "danhmuc/getDmCBYT";
  //public static CHECK_DM_DUOCPHAM: string = "CheckUpdate/CheckDateDuocPham";
 // public static GET_DM_DONVITINH: string = "danhmuc/getdmdonvitinh";
  //public static GET_DATE_DM_DONVITINH: string = "CheckUpdate/CheckDatedonvitinh";
  //public static GET_DM_DUONGDUNGDUOCPHAM: string = "danhmuc/getdmduongdungduocpham";
  //public static GET_DATE_DM_DUONGDUNGDUOCPHAM: string = "CheckUpdate/CheckDateduongdungduocpham";
  public static GET_CBYT: string = "cbyt/getcbyt";
  public static INSERT_CBYT: string = "cbyt/insertcbyt";
  public static UPDATE_CBYT: string = "cbyt/updatecbyt";

  public static GET_DM_NHATHUOC: string  = "danhmuc/getdmnhathuoc"
  //public static GET_CBYT: string = "cbyt/getcbyt";
  // Local

  public static TAB_LAYOUT_WEB: string = "../assets/layout_app/tabs.json";
  public static TAB_LAYOUT_MOBILE: string = "assets/layout_app/tabs.json";

  public static TYPE_MEDICAL_WEB: string = "../assets/data_example/type_medical.json";
  public static TYPE_MEDICAL_MOBILE: string = "assets/data_example/type_medical.json";
}


