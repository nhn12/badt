import { Injectable, NgZone } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Http, Response, RequestOptions, URLSearchParams } from '@angular/http'
import { ROUTE } from "../providers/const-route.ts";
import { SettingServices } from "../providers/setting-services"


/*
  Generated class for the RenderLayout provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class RenderLayout {

  tabs: Object[];
  typeMedicals: any[]
  constructor(public http: Http, public plt: Platform, private settingServices: SettingServices, private zone: NgZone) {
    this.tabs = [];
  }

  public loadTabs() {
    this.plt.ready().then(() => {

      this.settingServices.initDB();

      this.settingServices.getAll()
        .then(data => {
          this.zone.run(() => {
            if(data == undefined || data == null || data.length == 0)
            {
              this.getDataTabs();
            }
            else
            {
              let checkDataRemoteTab:boolean = false;
              data.forEach(element => {
              if (element.name == "dmtacvu") {
                checkDataRemoteTab = true;
                this.tabs = element.data; 
                this.sortTab();
              }
              // if(checkDataRemoteTab == false)
              // {
              //   this.getDataTabs();
              // }
            });
            }
            
          });
        })
        .catch(console.error.bind(console));
    });
  }

  private sortTab()
  {
    this.tabs.sort((a : any,b: any) : number=>{
                  if(a.serialtv > b.serialtv)
                  {
                    return 1;
                  }
                  if(a.serialtv < b.serialtv)
                  {
                    return -1;
                  }
                  return 0;
                });
  }

  private getDataTabs()
  {
    if (this.plt.is('ios') || this.plt.is('android')) {
      this.http.get(ROUTE.TAB_LAYOUT_MOBILE).subscribe(data => {
        this.tabs = data.json();
        this.sortTab();
      });
    }
    else {
      this.http.get(ROUTE.TAB_LAYOUT_WEB).subscribe(data => {
        this.tabs = data.json();
        this.sortTab();
      });
    }
  }

  public getTab() {
    return this.tabs;
  }

  public getTypeMedical() {
    if (this.plt.is('ios') || this.plt.is('android')) {
      return this.http.get(ROUTE.TYPE_MEDICAL_MOBILE);
    }
    else {
      return this.http.get(ROUTE.TYPE_MEDICAL_WEB);
    }

  }
}
