import { TabMainPage } from "../pages/tab-main/tab-main"
import { AlertController } from "ionic-angular";
import { LoadingServices } from "../providers/loading-services"
export class NavigationController {
  textTitleBack: any;
  iconBack: any;
  alertCtrl: any;
  constructor(public navCtrl, public appCtrl) {
    this.alertCtrl = new AlertController(appCtrl);
  }

  backPage() {
    if (this.navCtrl.getPrevious() != undefined) {
      
         this.navCtrl.pop();
      
     
    }
    else {

      let alert = this.alertCtrl.create({
        title: 'Kết thúc',
        message: 'Bạn có muốn kết thúc bệnh án?',
        buttons: [
          {
            text: 'Cancel',
            role: 'cancel',
            handler: () => {

            }
          },
          {
            text: 'OK',
            handler: () => {
              this.appCtrl.getRootNav().setRoot(TabMainPage);
            }
          }
        ]
      });
      alert.present();

    }
  }

  ionViewDidEnter() {
    let seft =this;
    setTimeout(function(){
      if (seft.navCtrl.getPrevious() != undefined) {
      seft.textTitleBack = seft.navCtrl.getPrevious().component.titlePage;
      seft.iconBack = "ios-arrow-dropleft";
      let abc = seft.navCtrl.getActive();
      let adefbc = seft.appCtrl.getActiveNav();
      if(seft.navCtrl.getActive().component.idPgae == "tv")
      {
        seft.textTitleBack = "Trang chủ";
      seft.iconBack = "ios-home-outline";
      }
    }
    else {
      seft.textTitleBack = "Trang chủ";
      seft.iconBack = "ios-home-outline";

    }
    }, 500);
    
  }
}