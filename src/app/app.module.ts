import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { BenhNhanMoiPage } from '../pages/benh-nhan-moi/benh-nhan-moi';
import { BenhSuPage } from '../pages/tac-vu-kham-benh/benh-su/benh-su';
//import { CacBoPhanPage } from '../pages/cac-bo-phan/cac-bo-phan';
import { CanLamSangPage } from '../pages/tac-vu-kham-benh/can-lam-sang/can-lam-sang';
//import { ChiDinhPage } from '../pages/chi-dinh/chi-dinh';
//import { ChiDinhKhacPage } from '../pages/chi-dinh-khac/chi-dinh-khac';
import { ChuanDoanPage } from '../pages/tac-vu-kham-benh/chuan-doan/chuan-doan';
import { DangNhapPage } from '../pages/dang-nhap/dang-nhap';
//import { DichVuKiThuatPage } from '../pages/dich-vu-ki-thuat/dich-vu-ki-thuat';
import { HanhChinhPage } from '../pages/tac-vu-kham-benh/hanh-chinh/hanh-chinh';
//import { KhamBenhPage } from '../pages/kham-benh/kham-benh';
import { SinhHieuPage } from '../pages/tac-vu-kham-benh/sinh-hieu/sinh-hieu';
//import { LiDoKhamBenhPage } from '../pages/li-do-kham-benh/li-do-kham-benh';
import { ThamKhaoToaThuocPage } from '../pages/tac-vu-kham-benh/tham-khao-toa-thuoc/tham-khao-toa-thuoc';
import { TienCanPage } from '../pages/tac-vu-kham-benh/tien-can/tien-can';
import { TimKiemPage } from '../pages/tim-kiem/tim-kiem';
import { ToaThuocPage } from '../pages/tac-vu-kham-benh/toa-thuoc/toa-thuoc';
import { TrangChuPage } from '../pages/trang-chu/trang-chu';
//import { VatTuSuDungPage } from '../pages/vat-tu-su-dung/vat-tu-su-dung';
import { CapNhatBenhNhanPage } from '../pages/cap-nhat-benh-nhan/cap-nhat-benh-nhan';
import { XuTriPage } from '../pages/tac-vu-kham-benh/xu-tri/xu-tri';
import { TempData } from "../providers/temp_data";
import { RenderLayout } from "../providers/render-layout";
import { RouteRemote } from "../providers/remote-services";
import { SettingServices } from "../providers/setting-services";
import { UserSettingPage } from "../pages/user-setting/user-setting";
import { TabMainPage } from "../pages/tab-main/tab-main";
import { TacVuPage } from "../pages/tac-vu/tac-vu";
import { AutocompleteTextTemplatePage } from "../template/autocomplete-text-template/autocomplete-text-template";
import { ChiDinhCBYTPage } from "../pages/tac-vu-kham-benh/chi-dinh-cbyt/chi-dinh-cbyt";
import {ChiDinhChamSocPage} from "../pages/tac-vu-kham-benh/chi-dinh-cham-soc/chi-dinh-cham-soc";
import {TacVuChamSocPage} from "../pages/tac-vu-kham-benh/tac-vu-cham-soc/tac-vu-cham-soc";
import {LamSangPage} from "../pages/tac-vu-kham-benh/lam-sang/lam-sang"
import {LoadingServices} from "../providers/loading-services"
import {BackNavService} from "../providers/back-nav-services";
import {MenuPatientFind} from "../template/menu-action-find-popover.ts"
import {ExtraActionToaThuoc} from "../pages/tac-vu-kham-benh/toa-thuoc/toa-thuoc"


@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    BenhNhanMoiPage,
    BenhSuPage,
    //CacBoPhanPage,
    CanLamSangPage,
    //ChiDinhKhacPage,
    //ChiDinhPage,
    ChuanDoanPage,
    DangNhapPage,
    //DichVuKiThuatPage,
    HanhChinhPage,
    //KhamBenhPage,
    SinhHieuPage,
    //LiDoKhamBenhPage,
    ThamKhaoToaThuocPage,
    TienCanPage,
    TimKiemPage,
    ToaThuocPage,
    TrangChuPage,
    //VatTuSuDungPage,
    XuTriPage,
    CapNhatBenhNhanPage,
    UserSettingPage,
    TabMainPage,
    TacVuPage,
    ChiDinhCBYTPage,
    AutocompleteTextTemplatePage, TacVuChamSocPage, ChiDinhChamSocPage, LamSangPage, MenuPatientFind, ExtraActionToaThuoc


  ],
  imports: [
    IonicModule.forRoot(MyApp, { iconMode: 'ios', pageTransition: 'ios' })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    BenhNhanMoiPage,
    BenhSuPage,
    //CacBoPhanPage,
    CanLamSangPage,
    //ChiDinhKhacPage,
    //ChiDinhPage,
    ChuanDoanPage,
    DangNhapPage,
    //DichVuKiThuatPage,
    HanhChinhPage,
   // KhamBenhPage,
    SinhHieuPage,
    //LiDoKhamBenhPage,
    ThamKhaoToaThuocPage,
    TienCanPage,
    TimKiemPage,
    ToaThuocPage,
    TrangChuPage,
   // VatTuSuDungPage,
    XuTriPage,
    CapNhatBenhNhanPage,
    UserSettingPage,
    TabMainPage,
    TacVuPage,
    ChiDinhCBYTPage,
    AutocompleteTextTemplatePage, ChiDinhChamSocPage, TacVuChamSocPage, LamSangPage, MenuPatientFind, ExtraActionToaThuoc
  ],
  providers: [{ provide: ErrorHandler, useClass: IonicErrorHandler }, TempData, RenderLayout, RouteRemote, SettingServices, LoadingServices, BackNavService]
})
export class AppModule { }
