import { ViewController } from "ionic-angular"
import { Component } from "@angular/core"
@Component({
    template: `
    <ion-list>
      <ion-item>
            <ion-icon name="md-person-add" item-left></ion-icon>
            Tra cứu bệnh án
            
        </ion-item>
        <ion-item>
            <ion-icon name="ios-create" item-left></ion-icon>
            Cập nhật thông tin BN
            
        </ion-item>
    
    </ion-list>
  `
})
export class MenuPatientFind {
    constructor(public viewCtrl: ViewController) { }

    close() {
        this.viewCtrl.dismiss();
    }
}