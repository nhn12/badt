import { Component , NgZone} from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { RouteRemote } from '../../providers/remote-services';
import {SettingServices} from "../../providers/setting-services"
import {DEFAULT_DIACRITICS} from "../../providers/unicode-define"

/*
  Generated class for the AutocompleteTextTemplate page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-autocomplete-text-template',
  templateUrl: 'autocomplete-text-template.html'
})
export class AutocompleteTextTemplatePage {

  autocompleteItems;
  listDataOriginal:any[];
  autocomplete: any;
  titlePage: any;
  titleSearch: any;
  nameItemDisplay: any;
  enableSubResult:any;
  delay:any;
  unicodeServices : any;
  constructor(public zone:NgZone,
  public navCtrl: NavController, 
  public navParams: NavParams, 
  private viewCtrl: ViewController, 
  private remoteService: RouteRemote, 
  public settingServices : SettingServices,
  ) {
    this.unicodeServices = new DEFAULT_DIACRITICS();
    this.autocomplete = {};
    this.autocomplete.query = '';
    this.titlePage = this.navParams.get("titlePage");
    this.titleSearch = this.navParams.get("titleSearch");
    if(this.navParams.get("typeSource") == "local")
    {
      this.settingServices.getAll()
      .then(data => {
        this.zone.run(() => {
          this.listDataOriginal = data.filter(x=>x.name==this.navParams.get("sourceData"))[0].data;
        })});
    }
    if(this.navParams.get("typeSource") == "remote")
    {
      this.navParams.get("sourceData").subscribe(data=>{
        this.listDataOriginal = data.json();
      });
    }
    this.enableSubResult = false;
    this.autocompleteItems = [];

      this.delay = (function(){
      var timer = 0;
      return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
      };
    })();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AutocompleteTextTemplatePage');
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

  chooseItem(item: any) {
    this.viewCtrl.dismiss(item);
  }

  updateSearch() {
    this.autocompleteItems = [];
    this.autocomplete.query = this.unicodeServices.convertUtf8ToAscii(this.autocomplete.query);
    if (this.autocomplete.query == '') {
      this.autocompleteItems = [];
      return;
    }
    switch (this.navParams.get("typeAction")) {
      case "dmicd":
        var self = this;
        this.delay(function(){
          self.autocompleteItems = self.listDataOriginal.filter(x=>x.search.indexOf(self.autocomplete.query)!=-1);
          if(self.autocompleteItems.length > 20)
          {
            self.autocompleteItems.splice(20, self.autocompleteItems.length -20);
          }
          self.autocompleteItems.forEach(element => {
            element.nameDisplay = element.tenicd;
          });
          
        }, 500 );
      
        break;
      case "dmbv": this.remoteService.searchDmBenhVien(this.autocomplete.query).subscribe(data => {
        this.autocompleteItems = data.json();
        this.autocompleteItems.forEach(element => {
          element.nameDisplay = element.tenbv;
        });

      });
        break;
      case "dmcbyt": 
   
        this.autocompleteItems = this.listDataOriginal.filter(x=>this.unicodeServices.convertUtf8ToAscii(x.tencbyt+" "+x.macbyt).toLowerCase().indexOf(this.autocomplete.query)!=-1);
        this.autocompleteItems.forEach(element => {
          element.nameDisplay = element.holotcbyt+element.tencbyt;
        });
        this.enableSubResult = true;
        break;
      case "dmnhathuoc":
        this.autocompleteItems = this.listDataOriginal.filter(x=>this.unicodeServices.convertUtf8ToAscii(x.tennt).toLowerCase().indexOf(this.autocomplete.query)!=-1);
        this.autocompleteItems.forEach(element => {
          element.nameDisplay = element.tennt;
        });
        //this.enableSubResult = true;
        break;

    }

  }

  ionViewDidEnter() {
    let elem = <HTMLInputElement>document.querySelector('.search-bar-focus input');
    if (elem) {
      elem.focus();
    }
  }
}
